/*
[eslint > Rule](https://eslint.org/docs/rules/)
[Prettier > A Prettier JavaScript Formatter](https://jlongster.com/A-Prettier-Formatter)
[reference > 프론트엔드 개발환경의 이해: 린트](https://jeonghwan-kim.github.io/series/2019/12/30/frontend-dev-env-lint.html)
[reference > Eslint 적용하기(React + Typescript + Prettier) | 플타 앞발자의 삽질로그](https://flamingotiger.github.io/javascript/eslint-setup/)
 */
module.exports = {
    extends: [
        'prettier',
        'airbnb',
        'airbnb/hooks',
        'prettier/react',
        'plugin:@typescript-eslint/recommended',
        'prettier/@typescript-eslint',
        'plugin:prettier/recommended',
        // 'plugin:i18next/recommended',
    ],
    parser: '@typescript-eslint/parser',
    plugins: ['prettier', '@typescript-eslint'],
    rules: {
        '@typescript-eslint/explicit-module-boundary-types': 0,
        'react/jsx-filename-extension': 0,
        'spaced-comment': 0,
        'no-console': 0,
        'import/no-unresolved': 0,
        'import/extensions': 0,
        'react/react-in-jsx-scope': 0,
        'import/no-extraneous-dependencies': 1,
        'react/jsx-props-no-spreading': 0,
        'react/prop-types': 0,
        'no-empty-pattern': 0,
        'no-shadow': 0,
        'no-new': 0,
        'jsx-a11y/click-events-have-key-events': 0,
        'jsx-a11y/no-static-element-interactions': 0,
        'react-hooks/exhaustive-deps': 0,
        'no-restricted-globals': 0,
        'react/button-has-type': 0,
        'prefer-default-export': 0,
        'no-param-reassign': 0,
        'import/prefer-default-export': 0,
        'no-use-before-define': 0,
        'react/style-prop-object': 0,
        'no-restricted-syntax': 0
        // '@typescript-eslint/no-unused-vars': 2,
        // 'destructuring-assignment': 0,
        // '@typescript-eslint/no-empty-function': 0,
    },
};
