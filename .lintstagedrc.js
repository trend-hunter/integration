module.exports = {
    '*.{ts,tsx}': [
        'prettier --write',
    ],
    '*.scss': [
        'stylelint --fix',
    ],
};