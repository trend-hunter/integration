/*
[reference > Stylelint 설정하기](https://velog.io/@kyusung/stylelint)
 */
module.exports = {
    extends: [
        'stylelint-config-standard',
        'stylelint-config-recommended',
        'stylelint-config-styled-components',
        'stylelint-config-recommended-scss',
    ],
    plugins: ['stylelint-scss', 'stylelint-order'],
    rules: {
        'at-rule-no-unknown': null,
        'scss/at-rule-no-unknown': true,
        'order/properties-alphabetical-order': true,
        'no-empty-source': null,
        'rule-empty-line-before': null,
        'selector-list-comma-newline-after': null,
        'no-descending-specificity': null,
        'value-no-vendor-prefix': null,
    },
};
