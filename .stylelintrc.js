/*
[reference > Stylelint 설정하기](https://velog.io/@kyusung/stylelint)
[reference > styled-components/stylelint-processor-styled-components: Lint your styled components with stylelint!](https://github.com/styled-components/stylelint-processor-styled-components)
 */
module.exports = {
    /*
    styled-component 도 하고싶은데 --fix 옵션 지원안해줘서 주석처리하고 묵힘
     */
    // processors: [['stylelint-processor-styled-components', {
    //     'moduleName': 'styled-components',
    //     'importName': 'default',
    //     'strict': false,
    //     'ignoreFiles': [],
    //     'parserPlugins': [
    //         'jsx',
    //         ['decorators', {'decoratorsBeforeExport': true}],
    //         'classProperties',
    //         'exportExtensions',
    //         'functionBind',
    //         'functionSent',
    //     ],
    // }]],
    extends: [
        'stylelint-config-standard',
        'stylelint-config-recommended',
        'stylelint-config-styled-components',
        'stylelint-config-recommended-scss',
    ],
    plugins: ['stylelint-scss', 'stylelint-order'],
    rules: {
        'at-rule-no-unknown': null,
        'scss/at-rule-no-unknown': true,
        'order/properties-alphabetical-order': true,
        'no-empty-source': null,
        'rule-empty-line-before': null,
        'selector-list-comma-newline-after': null,
        'no-descending-specificity': null,
        'block-no-empty': null,
        'value-no-vendor-prefix': null,
        'no-duplicate-selectors': null,
        'react/style-prop-object': null

        // 'selector-pseudo-class-no-unknown': null
    },
};
