# 시작하기 cli
```bash
# 패키지 인스톨
npm install
# 데브모드실행
npm run dev
# pre-commit action 인스톨
npx husky
```

# 임시 호스팅
```bash
npm install ngrok -g
ngrok http 3000
ngrok http 3001
```
