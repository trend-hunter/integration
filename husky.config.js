const tasks = (arr) => arr.join(' && ');

/*
[reference > lint-staged로 eslint 세상 편하게 자동화하기 | Huskyhoochu 기술 블로그](https://www.huskyhoochu.com/how-to-use-lint-staged/)
    pre-commit 커밋 메시지를 작성하기 전에 호출됨
    prepare-commit-msg 커밋 메시지 생성 후 편집기 실행 전에 호출됨
    commit-msg 커밋 메시지와 관련된 명령을 넣을 때 호출됨
    post-commit 커밋이 완료되면 호출됨
 */
/**
 * 커밋시 자동화 순서
 *  1. 빌드
 *  2. 빌드 git add
 *  3. lint 교정 및 검사
 * @type {{hooks: {'pre-commit': *}}}
 */
module.exports = {
    hooks: {
        'pre-commit': tasks([
            'lint-staged',
            'build-storybook',
            'npm run build',
            'git add ./public',
            'git add ./storybook-static',
        ]),
    },
};
