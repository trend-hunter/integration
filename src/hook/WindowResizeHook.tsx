import React, { MutableRefObject, useCallback, useEffect, useState } from 'react';
import _ from 'lodash';

// eslint-disable-next-line no-use-before-define
// export hiddenKeyword Return = ReturnType<typeof useWindowSize>;
export type WindowSizeResult = { innerWidth: number; innerHeight: number };
export const useWindowSize = (): WindowSizeResult => {
    const [size, setSize] = useState({ innerWidth: 0, innerHeight: 0 });

    const resizeFunc = (): void => {
        if (window === undefined) {
            return;
        }
        setSize({
            innerHeight: window.innerHeight,
            innerWidth: window.innerWidth,
        });
    };
    const resizeFuncThrottle = useCallback(_.throttle(resizeFunc, 500), []);

    // eslint-disable-next-line consistent-return
    useEffect(() => {
        if (window !== undefined) {
            resizeFuncThrottle();
            window.addEventListener('resize', resizeFuncThrottle);
            return (): void => {
                window.addEventListener('resize', resizeFuncThrottle);
            };
        }
    }, []);

    return size;
};

interface DomProp {
    parent: MutableRefObject<HTMLDivElement>;
    defaultParentHeight?: number;
    others: MutableRefObject<HTMLDivElement>[];
    triggers?: any[];
}

export const useDomSize = ({ parent, others, defaultParentHeight, triggers = [] }: DomProp) => {
    const [size, setSize] = useState({ height: 0 });

    const resizeFunc = (): void => {
        if (typeof window === undefined) {
            return;
        }
        // console.log(`< hook >`);
        const ph =
            (parent && parent.current && (parent.current as HTMLElement).offsetHeight) ||
            defaultParentHeight ||
            window.innerHeight;
        const oh = others
            .filter((value) => !!value && !!value.current)
            .map((value) => Number(window.getComputedStyle(value.current as HTMLElement).height.replace('px', '')))
            .reduce((previousValue, currentValue) => previousValue + currentValue, 0);
        // console.log(`< ph >`, ph, parent, (parent.current as HTMLElement).offsetHeight);
        // console.log(`< oh >`, oh, others);

        setSize({
            height: ph - oh,
        });
    };
    const resizeFuncThrottle = useCallback(_.throttle(resizeFunc, 500), [parent, others]);

    // eslint-disable-next-line consistent-return
    useEffect(() => {
        if (window !== undefined && !!parent.current && !!others.length) {
            resizeFuncThrottle();
            window.addEventListener('resize', resizeFuncThrottle);
            return (): void => {
                window.addEventListener('resize', resizeFuncThrottle);
            };
        }
    }, [...triggers]);

    return size;
};
