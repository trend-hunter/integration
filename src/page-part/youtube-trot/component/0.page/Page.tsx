// import MainFragment from '@page-part/youtube-trot/component/fragment/app-mrtrot/Mrtrot';
import SearchPortal from '@page-part/youtube-trot/component/layer/search/SearchLayer';
import VideoPortal from '@page-part/youtube-trot/component/layer/video/VideoLayer';
import { usePageRootStore } from '@page-part/youtube-trot/service/store/PageRootStoreConsumer';
import { observer } from 'mobx-react';
import React, { useEffect } from 'react';
import { initializeStore } from '@page-part/youtube-trot/service/store/PageRootStore';
import PageSearchQuery from '@page-part/youtube-trot/service/PageSearchQuery';
import PageLocalStorageService from '@page-part/youtube-trot/service/PageLocalStorageService';
import MainFragmentFactory from '@page-part/youtube-trot/component/fragment/MainFragmentFactory';
import Dev from '@page-part/youtube-trot/component/1.atom/Dev';
import { useWindowSize } from '../../../../hook/WindowResizeHook';
import sty from './Page.module.scss';

const Page = ({}) => {
    const windowSize = useWindowSize();
    const pageRootStore = usePageRootStore();

    useEffect(() => {
        pageRootStore.play.list.addApp(PageSearchQuery.app);
        pageRootStore.play.list.load();

        const link = JSON.parse(localStorage.getItem(PageLocalStorageService.getLinkKey()));
        const uiPortal = JSON.parse(localStorage.getItem(PageLocalStorageService.getUiPortalKey()));
        initializeStore({ uiPortal, link });
    }, []);

    const MainFragment = MainFragmentFactory();

    return (
        <>
            <div className={sty.main} style={{ height: `${windowSize.innerHeight}px` }}>
                <Dev />
                <div className={sty.slot}>
                    <MainFragment />
                    <SearchPortal />
                    <VideoPortal />
                </div>
            </div>
        </>
    );
};

export default observer(Page);
