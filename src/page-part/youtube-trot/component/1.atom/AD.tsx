import React, { MutableRefObject } from 'react';

interface Prop {
    selfRef?: MutableRefObject<HTMLDivElement>;
}

const AD = ({ selfRef }: Prop) => {
    return (
        <div
            ref={selfRef}
            style={{ background: 'white', zIndex: 1 }}
            onClick={(e) => {
                e.stopPropagation();
            }}
        >
            <ins
                className="kakao_ad_area"
                style={{ display: 'none' }}
                data-ad-unit="DAN-1ji4hixmks2ly"
                data-ad-width="320"
                data-ad-height="50"
            />
            <script type="text/javascript" src="//t1.daumcdn.net/kas/static/ba.min.js" async />
        </div>
    );
};

export default AD;
