import { usePageRootStore } from '@page-part/youtube-trot/service/store/PageRootStoreConsumer';
import EnvService from '@service/EnvService';
import { observer } from 'mobx-react';
import React, { MutableRefObject } from 'react';
import sty from './Dev.module.scss';

interface Prop {
    selfRef?: MutableRefObject<HTMLDivElement>;
}
const Dev = ({ selfRef }: Prop) => {
    const pageRootStore = usePageRootStore();

    return (
        <div ref={selfRef} style={{ zIndex: 1 }}>
            {EnvService.isDev && (
                <div
                    style={{
                        height: '10rem',
                        border: '3px solid black',
                        boxSizing: 'border-box',
                        background: 'aliceblue',
                        overflowY: 'auto',
                    }}
                    className={sty.main}
                >
                    <button
                        onClick={() => {
                            pageRootStore.play.list.getTestSrc().then((v) => {
                                console.log(`< 1 >`, v);
                            });
                        }}
                    >
                        yield
                    </button>
                    <div>----^</div>
                    <div>
                        {pageRootStore.play.list.testSrc.map((v) => (
                            <div key={v.id}>
                                {v.id} / {v.title}
                            </div>
                        ))}
                    </div>
                    {pageRootStore.play.list.testSrcLoading && <div>loading</div>}
                </div>
            )}
        </div>
    );
};

export default observer(Dev);
