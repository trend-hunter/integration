import { ButtonDefaultProp } from '@page-part/youtube-trot/component/1.atom/button/index';
import React from 'react';
import sty from './BtnFullHeight.module.scss';

const BtnFullHeight = ({ selfRef, onClick, style, cssClass, text, isDisabled }: ButtonDefaultProp) => {
    return (
        <button
            ref={selfRef}
            onClick={onClick}
            style={style}
            disabled={isDisabled}
            className={`${sty.main} ${cssClass}`}
        >
            {text}
        </button>
    );
};

export default BtnFullHeight;
