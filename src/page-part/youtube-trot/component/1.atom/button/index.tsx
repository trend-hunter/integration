import { CSSProperties, MutableRefObject } from 'react';

export * from './BtnFullHeight';

export interface ButtonDefaultProp {
    onClick: () => void;
    selfRef?: MutableRefObject<HTMLButtonElement>;
    style?: CSSProperties;
    cssClass?: string;
    text: string;
    isDisabled?: boolean;
}
