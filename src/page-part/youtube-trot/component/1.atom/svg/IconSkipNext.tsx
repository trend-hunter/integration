import { IconDefaultProp } from '@page-part/youtube-trot/component/1.atom/svg/index';
import React from 'react';

export const IconSkipNext = ({ className, style, onClick }: IconDefaultProp) => {
    return (
        <svg
            className={className}
            onClick={onClick}
            xmlns="http://www.w3.org/2000/svg"
            viewBox="0 0 24 24"
            fill="black"
            width="18px"
            height="18px"
        >
            <path d="M0 0h24v24H0z" fill="none" />
            <path d="M6 18l8.5-6L6 6v12zM16 6v12h2V6h-2z" />
        </svg>
    );
};
