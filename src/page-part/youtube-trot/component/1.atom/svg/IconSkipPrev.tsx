import { IconDefaultProp } from '@page-part/youtube-trot/component/1.atom/svg/index';
import React from 'react';

export const IconSkipPrev = ({ className, style, onClick }: IconDefaultProp) => {
    return (
        <svg
            className={className}
            onClick={onClick}
            xmlns="http://www.w3.org/2000/svg"
            viewBox="0 0 24 24"
            fill="black"
            width="18px"
            height="18px"
        >
            <path d="M0 0h24v24H0z" fill="none" />
            <path d="M6 6h2v12H6zm3.5 6l8.5 6V6z" />
        </svg>
    );
};
