import { CSSProperties } from 'react';

export * from '@page-part/youtube-trot/component/1.atom/svg/IconPlayIng';
export * from '@page-part/youtube-trot/component/1.atom/svg/IconSkipNext';
export * from '@page-part/youtube-trot/component/1.atom/svg/IconSkipPrev';
export * from '@page-part/youtube-trot/component/1.atom/svg/IconPlay';
export * from '@page-part/youtube-trot/component/1.atom/svg/IconPause';

export interface IconDefaultProp {
    style?: CSSProperties;
    className?: string;
    onClick?: () => void;
}
