import * as Icon from '@page-part/youtube-trot/component/1.atom/svg';
import ItemModel from '@page-part/youtube-trot/service/model/ItemModel';
import React, { CSSProperties, MutableRefObject } from 'react';
import { AnimatePresence, motion } from 'framer-motion';

import classnames from 'classnames';
import sty from './Item.module.scss';

interface Prop {
    item: ItemModel;
    onClick: () => void;
    isPlay?: boolean;
    isSelected?: boolean;
    selfRef: MutableRefObject<HTMLDivElement>;
    style?: CSSProperties;
}
const Item = ({ style, item, onClick, isPlay, selfRef, isSelected }: Prop) => {
    const cssClass = classnames(sty.main, { [sty.isPlay]: isPlay, [sty.isSelected]: isSelected });
    const clickHandler = () => {
        onClick();
    };
    return (
        <div
            // layout
            //
            className={cssClass}
            onClick={clickHandler}
            ref={selfRef}
            style={{ ...style }}
        >
            <div className={sty.col1}>
                <img src={item.thumbnail} alt="" />
            </div>
            <div className={sty.col2}>
                <div className={sty.title}>
                    {item.title} - {item.singer.join(',')}
                </div>
                <div className={sty.type}>
                    {[...item.hiddenKeyword, ...item.app].map((value) => (
                        <span className={sty.typeItem}>
                            <span className={sty.hash}>#</span>
                            {value}
                        </span>
                    ))}
                </div>
            </div>
            <AnimatePresence>
                {isPlay && (
                    <motion.div
                        layout
                        layoutId="IconPlayIng"
                        style={{ zIndex: 10000 }}
                        //
                        animate={{ opacity: 1 }}
                    >
                        <Icon.IconPlayIng
                            className={sty.iconIng}
                            style={{ width: '1.4rem', height: '100%', alignSelf: 'center', marginRight: '0.4rem' }}
                        />
                    </motion.div>
                )}
            </AnimatePresence>
        </div>
    );
};

export default Item;
