import BtnFullHeight from '@page-part/youtube-trot/component/1.atom/button/BtnFullHeight';
import FooterTemplate from '@page-part/youtube-trot/component/4.template/FooterTemplate';
import { usePageRootStore } from '@page-part/youtube-trot/service/store/PageRootStoreConsumer';
import { observer } from 'mobx-react';
import React, { MutableRefObject } from 'react';
import sty from './FooterControllerDelete.module.scss';

interface Prop {
    selfRef?: MutableRefObject<HTMLDivElement>;
}

const FooterControllerDelete = ({ selfRef }: Prop) => {
    const pageRootStore = usePageRootStore();
    const deleteAll = () => {
        pageRootStore.ui.setMainFragmentCurrIndex(0);
        pageRootStore.play.link.clearSelected();
        pageRootStore.play.link.clearIngIdMap();
        pageRootStore.play.link.setCurrId('');
        pageRootStore.ui.setSentence('');
    };
    return (
        <FooterTemplate className={sty.main} selfRef={selfRef}>
            <BtnFullHeight
                onClick={deleteAll}
                isDisabled={!pageRootStore.play.list.ingList.length}
                text="전체 삭제"
                style={{ borderRadius: '0.8rem' }}
            />
        </FooterTemplate>
    );
};

export default observer(FooterControllerDelete);
