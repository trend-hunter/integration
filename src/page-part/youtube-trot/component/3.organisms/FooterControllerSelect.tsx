import BtnFullHeight from '@page-part/youtube-trot/component/1.atom/button/BtnFullHeight';
import ItemModel from '@page-part/youtube-trot/service/model/ItemModel';
import { usePageRootStore } from '@page-part/youtube-trot/service/store/PageRootStoreConsumer';
import { observer } from 'mobx-react';
import React, { MutableRefObject, useState } from 'react';
import VideoPolicyPortal from '@page-part/youtube-trot/component/portal/VideoPolicyPortal';
import FooterTemplate from '../4.template/FooterTemplate';
import sty from './FooterControllerSelect.module.scss';

interface Prop {
    selfRef?: MutableRefObject<HTMLDivElement>;
    allItems: ItemModel[];
}
const FooterControllerSelect = ({ selfRef, allItems }: Prop) => {
    const pageRootStore = usePageRootStore();
    const [isShowSelect, setIsShowSelect] = useState(false);
    const [isShowAll, setIsShowAll] = useState(false);

    const playSelect = () => {
        const id = pageRootStore.play.link.putSelectedToIng();
        pageRootStore.play.link.setCurrId(id);
        pageRootStore.ui.togglePlayer(true);
        pageRootStore.ui.toggleSearch(false);
        pageRootStore.ui.setMainFragmentCurrIndex(1);
        pageRootStore.ui.setSentence('');
    };
    const playAll = () => {
        const ids = Array.from(allItems.map((value) => value.id)).reverse();
        pageRootStore.play.link.putAllToIng(ids);
        pageRootStore.ui.togglePlayer(true);
        pageRootStore.ui.toggleSearch(false);
        pageRootStore.ui.setMainFragmentCurrIndex(1);
        pageRootStore.ui.setSentence('');
    };
    return (
        <>
            <FooterTemplate className={sty.main} selfRef={selfRef}>
                <BtnFullHeight
                    isDisabled={!pageRootStore.play.link.selectedIdMap.size}
                    onClick={() => {
                        if (pageRootStore.ui.portal.isShowVideoPolicyPortalDisable) {
                            playSelect();
                        } else {
                            setIsShowSelect(true);
                        }
                    }}
                    text="선택 재생"
                    style={{ borderRadius: '0.8rem' }}
                />
                <BtnFullHeight
                    onClick={() => {
                        if (pageRootStore.ui.portal.isShowVideoPolicyPortalDisable) {
                            playAll();
                        } else {
                            setIsShowAll(true);
                        }
                    }}
                    text="전체 재생"
                    style={{ borderRadius: '0.8rem' }}
                />
            </FooterTemplate>
            {isShowSelect && (
                <VideoPolicyPortal
                    cancelCallback={() => {
                        setIsShowSelect(false);
                    }}
                    okCallback={playSelect}
                />
            )}
            {isShowAll && (
                <VideoPolicyPortal
                    cancelCallback={() => {
                        setIsShowAll(false);
                    }}
                    okCallback={playAll}
                />
            )}
        </>
    );
};

export default observer(FooterControllerSelect);
