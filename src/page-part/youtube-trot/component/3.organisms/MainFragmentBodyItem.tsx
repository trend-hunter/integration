import React, { CSSProperties, MutableRefObject, useRef } from 'react';
import { usePageRootStore } from '@page-part/youtube-trot/service/store/PageRootStoreConsumer';
import classnames from 'classnames';
import { observer } from 'mobx-react';
import sty from './MainFragmentBodyItem.module.scss';

const MainFragmentBodyItem = ({
    children,
    selfRef,
    style,
    footer,
    index,
}: {
    style?: CSSProperties;
    children: any;
    footer?: JSX.Element;
    selfRef?: MutableRefObject<HTMLDivElement>;
    index: number;
}) => {
    const footerRef = useRef();
    const pageRootStore = usePageRootStore();
    const cssClass = classnames(sty.bodyItem, { [sty.isSelected]: pageRootStore.ui.mainFragmentCurrIndex === index });

    return (
        <div className={cssClass} ref={selfRef} style={{ ...style }}>
            <div className={sty.wrap} style={{}}>
                <div className={sty.body} style={{}}>
                    {children}
                </div>
                <div className={sty.footer} ref={footerRef}>
                    {footer}
                </div>
            </div>
        </div>
    );
};

export default observer(MainFragmentBodyItem);
