import BtnFullHeight from '@page-part/youtube-trot/component/1.atom/button/BtnFullHeight';
import HeaderTemplate from '@page-part/youtube-trot/component/4.template/HeaderTemplate';
import { usePageRootStore } from '@page-part/youtube-trot/service/store/PageRootStoreConsumer';
import { observer } from 'mobx-react';
import React, { MutableRefObject, useRef } from 'react';
import PageSearchQuery from '@page-part/youtube-trot/service/PageSearchQuery';

interface Prop {
    selfRef?: MutableRefObject<HTMLDivElement>;
}

const MainFragmentHeader = ({ selfRef }: Prop) => {
    const rightRef = useRef();
    const pageRootStore = usePageRootStore();
    return (
        <HeaderTemplate
            selfRef={selfRef}
            right={
                <BtnFullHeight
                    selfRef={rightRef}
                    onClick={() => {
                        pageRootStore.ui.toggleSearch(true);
                    }}
                    text="검색하기"
                    style={{ borderRadius: '0 0 0 0.6rem', width: '100%' }}
                />
            }
            left={
                !!pageRootStore.play.link.currId && (
                    <BtnFullHeight
                        selfRef={rightRef}
                        onClick={() => {
                            pageRootStore.ui.togglePlayer(true);
                        }}
                        text="뒤로가기"
                        style={{ borderRadius: '0 0 0.6rem 0', width: '100%' }}
                    />
                )
            }
            center={PageSearchQuery.app[0] || '트로트'}
        />
    );
};

export default observer(MainFragmentHeader);
