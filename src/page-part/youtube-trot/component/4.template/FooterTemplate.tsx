import React, { CSSProperties, MutableRefObject } from 'react';
import sty from './FooterTemplate.module.scss';

interface Prop {
    selfRef?: MutableRefObject<HTMLDivElement>;
    className?: string;
    children: any;
}
const FooterTemplate = ({ selfRef, children, className }: Prop) => {
    return (
        <div className={`${sty.main} ${className}`} ref={selfRef}>
            {children}
        </div>
    );
};

export default FooterTemplate;
