import Tab from '@material-ui/core/Tab';
import Tabs from '@material-ui/core/Tabs';
import FooterControllerDelete from '@page-part/youtube-trot/component/3.organisms/FooterControllerDelete';
import Header from '@page-part/youtube-trot/component/3.organisms/MainFragmentHeader';
import ItemsPlaying from '@page-part/youtube-trot/component/item/ItemsPlaying';
import { usePageRootStore } from '@page-part/youtube-trot/service/store/PageRootStoreConsumer';
import { observer } from 'mobx-react';
import React from 'react';
import BodyItem from '@page-part/youtube-trot/component/3.organisms/MainFragmentBodyItem';
import { useDrag } from 'react-use-gesture';
import sty from './FragmenteTemplate.module.scss';

export interface TabInfo {
    label: string;
    footer: JSX.Element;
    body: JSX.Element;
}
interface Prop {
    firstTabInfo: TabInfo;
    tabInfos: TabInfo[];
}

const FragmentTemplate = ({ firstTabInfo, tabInfos }: Prop) => {
    const pageRootStore = usePageRootStore();
    const firstStaticTabSize = 2;
    const lastTabIndex = tabInfos.length + firstStaticTabSize;
    const isDisabledMyTab = !pageRootStore.play.list.ingList.length;
    const changeHandler = (v: number) => {
        if (lastTabIndex === v && !pageRootStore.play.list.ingList.length) {
            return;
        }
        pageRootStore.ui.setMainFragmentCurrIndex(v);
        pageRootStore.play.link.clearSelected();
    };

    const bind = useDrag(({ down, movement: [mx, my], direction: [xDir], distance, cancel }) => {
        if (Math.abs(my) > 50) {
            return;
        }
        const triggerNumber = (window.innerWidth / 10) * 2;
        if (!down && triggerNumber < mx) {
            if (pageRootStore.ui.mainFragmentCurrIndex === 2 && isDisabledMyTab) {
                changeHandler(0);
            } else {
                changeHandler(Math.max(0, pageRootStore.ui.mainFragmentCurrIndex - 1));
            }
        } else if (!down && -triggerNumber > mx) {
            if (pageRootStore.ui.mainFragmentCurrIndex === 0 && isDisabledMyTab) {
                changeHandler(2);
            } else {
                changeHandler(
                    Math.min(tabInfos.length + firstStaticTabSize - 1, pageRootStore.ui.mainFragmentCurrIndex + 1),
                );
            }
        }
    });

    return (
        <div className={sty.main} {...bind()}>
            <Header />
            <style global jsx>{`
                .MuiTabs-flexContainer {
                    //justify-content: center;
                }
            `}</style>
            <Tabs
                value={pageRootStore.ui.mainFragmentCurrIndex}
                onChange={(e, v) => {
                    changeHandler(v);
                }}
                className={sty.tabs}
                style={{ paddingBottom: '6px' }}
                variant="scrollable"
            >
                <Tab label={firstTabInfo.label} className={sty.tab} />
                <Tab label="내목록" className={sty.tab} />
                {tabInfos.map((value, index) => (
                    // eslint-disable-next-line react/no-array-index-key
                    <Tab key={`${value.label}${index}`} label={value.label} className={sty.tab} />
                ))}
            </Tabs>
            <div className={sty.body}>
                <BodyItem key={firstTabInfo.label} index={0} footer={firstTabInfo.footer}>
                    {firstTabInfo.body}
                </BodyItem>
                <BodyItem index={1} footer={<FooterControllerDelete />}>
                    <ItemsPlaying isDrag />
                </BodyItem>
                {tabInfos.map((value, index) => (
                    <BodyItem key={value.label} index={index + 2} footer={value.footer}>
                        {value.body}
                    </BodyItem>
                ))}
            </div>
        </div>
    );
};

export default observer(FragmentTemplate);
