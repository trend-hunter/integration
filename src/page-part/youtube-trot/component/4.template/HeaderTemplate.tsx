import { usePageRootStore } from '@page-part/youtube-trot/service/store/PageRootStoreConsumer';
import StyleUtil from '@util/StyleUtil';
import { observer } from 'mobx-react';
import React, { MutableRefObject, useCallback, useEffect, useRef, useState } from 'react';
import _ from 'lodash';
import sty from './HeaderTemplate.module.scss';

interface Prop {
    center: any;
    left?: JSX.Element;
    right?: JSX.Element;
    selfRef?: MutableRefObject<HTMLDivElement>;
}

const HeaderTemplate = ({ selfRef, center, left, right }: Prop) => {
    const pageRootStore = usePageRootStore();
    const leftRef = useRef();
    const rightRef = useRef();
    const [maxSideWidth, setMaxSideWidth] = useState(0);

    useEffect(() => {
        const leftWidth = StyleUtil.getComputedWidth(leftRef.current as HTMLElement);
        const rightWidth = StyleUtil.getComputedWidth(rightRef.current as HTMLElement);
        setMaxSideWidth(Math.max(leftWidth, rightWidth));
    }, [pageRootStore.ui.isPlayerOpen]);

    const [cnt, setCnt] = useState(0);
    const clearCnt = useCallback(
        _.debounce(() => {
            setCnt(0);
        }, 3000),
        [],
    );

    const getHost = (host: string) => {
        const master = 'https://trend-hunter-web-integration.vercel.app';
        const release = 'https://trend-hunter-web-integration-git-release.zsgg.vercel.app';
        const local = 'http://localhost:3000';
        const urlArr = [master, release, local];
        const urlArrIndex = [1, 0, 0];
        return urlArr[
            urlArrIndex[
                Math.max(
                    urlArr.findIndex((value) => value === host),
                    0,
                )
            ]
        ];
    };
    const clickHandler = () => {
        setCnt(cnt + 1);
        clearCnt();
        if (cnt !== 6) {
            return;
        }
        const uri = '?';
        const urlSplit = location.href.split(uri);
        if (!urlSplit.length) {
            return;
        }
        const urlHost = urlSplit[0];
        const urlQueryString = urlSplit[1] ? `?${urlSplit[1]}` : '';
        location.href = `${getHost(urlHost)}${urlQueryString}`;
    };

    return (
        <div className={sty.main} ref={selfRef} onClick={clickHandler}>
            <div className={sty.left} ref={leftRef} style={{ width: `${maxSideWidth}px` }}>
                {!!left && left}
            </div>
            <div className={sty.center}>{center}</div>
            <div className={sty.right} ref={rightRef} style={{ width: `${maxSideWidth}px`, minWidth: '6rem' }}>
                {!!right && right}
            </div>
        </div>
    );
};

export default observer(HeaderTemplate);
