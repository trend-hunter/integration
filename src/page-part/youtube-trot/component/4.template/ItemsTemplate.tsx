import { usePageRootStore } from '@page-part/youtube-trot/service/store/PageRootStoreConsumer';
import { observer } from 'mobx-react';
import React, { CSSProperties, MutableRefObject, useEffect, useRef } from 'react';
import { AnimateSharedLayout } from 'framer-motion';
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore

interface Prop {
    items: (ref: MutableRefObject<any>) => JSX.Element[];
    style?: CSSProperties;
}

const ItemsTemplate = ({ items, style }: Prop) => {
    const selfRef = useRef();
    const selectedItemRef = useRef();
    const pageRootStore = usePageRootStore();
    /*
    선택된 아이템으로 스크롤 업
    */
    useEffect(() => {
        if (!selfRef.current || !selectedItemRef.current) {
            return;
        }

        const { offsetTop, offsetHeight } = selectedItemRef.current as HTMLElement;
        const top = offsetTop - offsetHeight;
        (selfRef.current as HTMLElement).scrollTo({ top, behavior: 'smooth' });
        (selfRef.current as HTMLElement).parentElement.scrollTo({ top, behavior: 'smooth' });
    }, [pageRootStore.play.link.currId, pageRootStore.ui.isPlayerOpen]);

    return (
        <AnimateSharedLayout type="crossfade">
            <div ref={selfRef} style={{ overflowY: 'auto', position: 'relative', height: `auto`, ...style }}>
                {items(selectedItemRef)}
            </div>
        </AnimateSharedLayout>
    );
};

export default observer(ItemsTemplate);
