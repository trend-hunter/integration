import React, { CSSProperties, MutableRefObject } from 'react';
import classnames from 'classnames';
import sty from './OverlayTemplate.module.scss';

interface Prop {
    children?: any;
    selfRef?: MutableRefObject<HTMLDivElement>;
    isShow: boolean;
    className?: string;
    style?: CSSProperties;
}
const OverlayTemplate = ({ isShow, children, selfRef, style }: Prop) => {
    const cssClass = classnames(sty.main, { [sty.isShow]: isShow });
    return (
        <>
            <div ref={selfRef} className={cssClass} style={style}>
                {children}
            </div>
        </>
    );
};

export default OverlayTemplate;
