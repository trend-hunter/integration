import React, { CSSProperties, MutableRefObject, useEffect, useRef, useState } from 'react';
import { createPortal } from 'react-dom';
import OverflowHidden from '@page-part/youtube-trot/component/x.xxx/OverflowHidden';
import { PortalRootId } from '../../../../Const';
import sty from './PortalTemplate.module.scss';
import { FunctionReturnVoid } from '../../../../Types';

interface Prop {
    children?: any;
    setIsShowRef?: MutableRefObject<(bo: boolean) => void>;
    style?: CSSProperties;
    closeHandler: FunctionReturnVoid;
}

const PortalTemplate = ({ children, closeHandler, style }: Prop) => {
    const ref = useRef();
    const [mounted, setMounted] = useState(false);

    useEffect(() => {
        const root = document.getElementById(PortalRootId);
        if (root) {
            (ref.current as HTMLElement) = root as HTMLElement;
            setMounted(true);
        }
    }, []);

    return mounted
        ? createPortal(
              <>
                  <OverflowHidden />
                  <div className={sty.overlay} onClick={closeHandler}>
                      <div
                          style={style}
                          className={sty.main}
                          onClick={(e) => {
                              e.stopPropagation();
                          }}
                      >
                          {children}
                      </div>
                  </div>
              </>,
              ref.current,
          )
        : null;
};

export default PortalTemplate;
