import FooterControllerSelect from '@page-part/youtube-trot/component/3.organisms/FooterControllerSelect';
import { usePageRootStore } from '@page-part/youtube-trot/service/store/PageRootStoreConsumer';
import { observer } from 'mobx-react';
import React from 'react';
import ItemsDefault from '@page-part/youtube-trot/component/item/ItemsDefault';
import FragmentTemplate from '@page-part/youtube-trot/component/4.template/FragmentTemplate';

const FragmentIm = () => {
    const pageRootStore = usePageRootStore();
    const mrtrot = pageRootStore.play.list.getFilteredPlaylist({
        filter: (v) => {
            return v.app.indexOf('미스터트롯') > -1 && v.singer.indexOf('임영웅') > -1;
        },
    });
    const ppongsoong = pageRootStore.play.list.getFilteredPlaylist({
        filter: (v) => {
            return v.app.indexOf('뽕숭아학당') > -1 && v.singer.indexOf('임영웅') > -1;
        },
    });

    return (
        <FragmentTemplate
            firstTabInfo={{
                label: '전체',
                body: <ItemsDefault models={pageRootStore.play.list.srcCategoryValues} />,
                footer: <FooterControllerSelect allItems={pageRootStore.play.list.srcCategoryValues} />,
            }}
            tabInfos={[
                {
                    label: '미스터트롯',
                    body: <ItemsDefault models={mrtrot} />,
                    footer: <FooterControllerSelect allItems={mrtrot} />,
                },
                {
                    label: '뽕숭아학당',
                    body: <ItemsDefault models={ppongsoong} />,
                    footer: <FooterControllerSelect allItems={ppongsoong} />,
                },
            ]}
        />
    );
};

export default observer(FragmentIm);
