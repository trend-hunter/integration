import FooterControllerSelect from '@page-part/youtube-trot/component/3.organisms/FooterControllerSelect';
import { usePageRootStore } from '@page-part/youtube-trot/service/store/PageRootStoreConsumer';
import { observer } from 'mobx-react';
import React from 'react';
import ItemsDefault from '@page-part/youtube-trot/component/item/ItemsDefault';
import FragmentTemplate from '@page-part/youtube-trot/component/4.template/FragmentTemplate';

const FragmentMrtrot = () => {
    const pageRootStore = usePageRootStore();
    const { tabPlaylist, tabTitle } = pageRootStore.play.list.srcCategoryTabs;

    return (
        <FragmentTemplate
            firstTabInfo={{
                label: '전체',
                body: <ItemsDefault models={pageRootStore.play.list.srcCategoryValues} />,
                footer: <FooterControllerSelect allItems={pageRootStore.play.list.srcCategoryValues} />,
            }}
            tabInfos={[
                {
                    label: tabTitle[0],
                    body: <ItemsDefault models={tabPlaylist[0]} />,
                    footer: <FooterControllerSelect allItems={tabPlaylist[0]} />,
                },
                {
                    label: tabTitle[1],
                    body: <ItemsDefault models={tabPlaylist[1]} />,
                    footer: <FooterControllerSelect allItems={tabPlaylist[1]} />,
                },
            ]}
        />
    );
};

export default observer(FragmentMrtrot);
