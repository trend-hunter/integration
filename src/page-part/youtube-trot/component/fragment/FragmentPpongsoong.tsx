import FooterControllerSelect from '@page-part/youtube-trot/component/3.organisms/FooterControllerSelect';
import { usePageRootStore } from '@page-part/youtube-trot/service/store/PageRootStoreConsumer';
import { observer } from 'mobx-react';
import React from 'react';
import ItemsDefault from '@page-part/youtube-trot/component/item/ItemsDefault';
import FragmentTemplate, { TabInfo } from '@page-part/youtube-trot/component/4.template/FragmentTemplate';

const FragmentIm = () => {
    const pageRootStore = usePageRootStore();
    const types = ['임영웅', '영탁', '장민호', '이찬원'];
    const tabInfos: TabInfo[] = types.map((value) => {
        const tab = pageRootStore.play.list.getFilteredPlaylist({
            filter: (v) => {
                return v.app.indexOf('뽕숭아학당') > -1 && v.singer.indexOf(value) > -1;
            },
        });
        tab.sort((a, b) => {
            return a.singer.length - b.singer.length;
        });
        return {
            label: value,
            body: <ItemsDefault models={tab} />,
            footer: <FooterControllerSelect allItems={tab} />,
        };
    });

    return (
        <FragmentTemplate
            firstTabInfo={{
                label: '전체',
                body: <ItemsDefault models={pageRootStore.play.list.srcCategoryValues} />,
                footer: <FooterControllerSelect allItems={pageRootStore.play.list.srcCategoryValues} />,
            }}
            tabInfos={tabInfos}
        />
    );
};

export default observer(FragmentIm);
