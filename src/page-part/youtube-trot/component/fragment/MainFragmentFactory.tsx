import React from 'react';
import Mrtrot from '@page-part/youtube-trot/component/fragment/FragmentMrtrot';
import PageSearchQuery from '@page-part/youtube-trot/service/PageSearchQuery';
import Im from '@page-part/youtube-trot/component/fragment/FragmentIm';
import Ppongsoong from '@page-part/youtube-trot/component/fragment/FragmentPpongsoong';

type Value = () => JSX.Element;
const labelMap = new Map<string, Value>();

//todo enum 으로 변경
labelMap.set('미스터트롯', Mrtrot);
labelMap.set('임영웅', Im);
labelMap.set('뽕숭아학당', Ppongsoong);

// eslint-disable-next-line consistent-return
const MainFragmentFactory = () => {
    const label = PageSearchQuery.app[0];
    const value = labelMap.get(label);
    if (value) {
        return value;
    }
    return Mrtrot;
};

export default MainFragmentFactory;
