import BtnFullHeight from '@page-part/youtube-trot/component/1.atom/button/BtnFullHeight';
import Item from '@page-part/youtube-trot/component/2.molecules/Item';
import ItemModel from '@page-part/youtube-trot/service/model/ItemModel';
import { usePageRootStore } from '@page-part/youtube-trot/service/store/PageRootStoreConsumer';
import React, { MutableRefObject, useRef, useState } from 'react';
import classnames from 'classnames';
import { SortableElement, SortableHandle } from 'react-sortable-hoc';
import sty from './ItemPlaying.module.scss';

interface Prop {
    item: ItemModel;
    onClick: () => void;
    isPlay?: boolean;
    selfRef: MutableRefObject<HTMLDivElement>;
    isDrag: boolean;
}
const ItemPlaying = ({ item, onClick, isPlay, selfRef, isDrag }: Prop) => {
    const pageRootStore = usePageRootStore();
    const [isShow, setIsShow] = useState(true);
    const cssClass = classnames(sty.main, { [sty.isHide]: !isShow });
    const wrapRef = useRef();

    return (
        <>
            <div ref={wrapRef} key={item.id} className={cssClass}>
                <div>{isDrag && <Handle />}</div>
                <Item
                    style={{ margin: '0' }}
                    onClick={onClick}
                    selfRef={selfRef}
                    item={item}
                    isSelected={false}
                    isPlay={isPlay}
                />
                <div className={sty.btnDelete}>
                    <BtnFullHeight
                        onClick={() => {
                            setIsShow(false);
                            setTimeout(() => {
                                (wrapRef.current as HTMLElement).style.display = 'none';
                                pageRootStore.play.link.removeIngId(item);
                            }, 500);
                        }}
                        text="삭제"
                    />
                </div>
            </div>
        </>
    );
};

const Handle = SortableHandle(() => (
    <div className={sty.handle}>
        <svg viewBox="0 0 50 50">
            <path
                d="M 0 7.5 L 0 12.5 L 50 12.5 L 50 7.5 L 0 7.5 z M 0 22.5 L 0 27.5 L 50 27.5 L 50 22.5 L 0 22.5 z M 0 37.5 L 0 42.5 L 50 42.5 L 50 37.5 L 0 37.5 z"
                color="#000"
            />
        </svg>
    </div>
));

const ItemPlayingWithSortableElement = SortableElement((prop: Prop) => {
    return <ItemPlaying {...prop} />;
});

export default ItemPlayingWithSortableElement;
