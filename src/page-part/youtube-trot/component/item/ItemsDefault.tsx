import Item from '@page-part/youtube-trot/component/2.molecules/Item';
import ItemsTemplate from '@page-part/youtube-trot/component/4.template/ItemsTemplate';
import ItemModel from '@page-part/youtube-trot/service/model/ItemModel';
import { usePageRootStore } from '@page-part/youtube-trot/service/store/PageRootStoreConsumer';
import { observer } from 'mobx-react';
import React from 'react';

interface Prop {
    models: ItemModel[];
    onSelected?: (v: ItemModel) => boolean;
    onClick?: (v: ItemModel) => void;
}

const ItemsDefault = ({ models = [], onSelected, onClick }: Prop) => {
    const pageRootStore = usePageRootStore();
    const clickHandler =
        onClick ||
        ((v) => {
            pageRootStore.play.link.toggleSelected(v);
        });
    const isSelectedHandler =
        onSelected ||
        ((v) => {
            return pageRootStore.play.link.selectedIdMap.has(v.id);
        });

    return (
        <ItemsTemplate
            items={(ref) => {
                return models.map((v) => {
                    return (
                        <Item
                            item={v as ItemModel}
                            key={v.id}
                            isPlay={v.id === pageRootStore.play.link.currId}
                            isSelected={isSelectedHandler(v)}
                            onClick={() => {
                                clickHandler(v);
                            }}
                            selfRef={v.id === pageRootStore.play.link.currId ? ref : null}
                        />
                    );
                });
            }}
        />
    );
};

export default observer(ItemsDefault);
