import ItemPlaying from '@page-part/youtube-trot/component/item/ItemPlaying';
import ItemsTemplate from '@page-part/youtube-trot/component/4.template/ItemsTemplate';
import { usePageRootStore } from '@page-part/youtube-trot/service/store/PageRootStoreConsumer';
import React, { CSSProperties } from 'react';
import { SortableContainer } from 'react-sortable-hoc';

interface Prop {
    style?: CSSProperties;
    isDrag: boolean;
}

const ItemsPlaying = ({ style, isDrag }: Prop) => {
    const pageRootStore = usePageRootStore();
    const models = pageRootStore.play.list.ingList;

    return (
        <ItemsTemplate
            style={{ ...style, overflow: 'hidden' }}
            items={(ref) => {
                return models.map((v, index) => {
                    return (
                        <ItemPlaying
                            isDrag={isDrag}
                            index={index}
                            item={v}
                            key={v.id}
                            isPlay={v.id === pageRootStore.play.link.currId}
                            onClick={() => {
                                pageRootStore.play.link.setCurrId(v.id);
                                pageRootStore.ui.togglePlayer(true);
                            }}
                            selfRef={v.id === pageRootStore.play.link.currId ? ref : null}
                        />
                    );
                });
            }}
        />
    );
};

const ItemsPlayingWithSortableContainer = SortableContainer(ItemsPlaying);
const ItemsPlayingWithSortableContainerWithConfig = ({ isDrag = false }: { isDrag: boolean }) => {
    const pageRootStore = usePageRootStore();
    const onSortEnd = ({ oldIndex, newIndex }) => {
        pageRootStore.play.link.switchPositionIngMap(oldIndex, newIndex);
    };

    return (
        <ItemsPlayingWithSortableContainer
            onSortEnd={onSortEnd}
            axis="y"
            lockAxis="y"
            useWindowAsScrollContainer
            useDragHandle
            isDrag={isDrag}
        />
    );
};

export default ItemsPlayingWithSortableContainerWithConfig;
