import ItemsDefault from '@page-part/youtube-trot/component/item/ItemsDefault';
import ItemModel from '@page-part/youtube-trot/service/model/ItemModel';
import { observer } from 'mobx-react';
import React from 'react';

interface Prop {
    items: ItemModel[];
}
const ItemsSearch = ({ items }: Prop) => {
    return <ItemsDefault models={items} />;
};

export default observer(ItemsSearch);
