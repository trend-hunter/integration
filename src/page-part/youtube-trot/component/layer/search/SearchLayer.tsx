import BtnFullHeight from '@page-part/youtube-trot/component/1.atom/button/BtnFullHeight';
import SelectController from '@page-part/youtube-trot/component/3.organisms/FooterControllerSelect';
import HeaderTemplate from '@page-part/youtube-trot/component/4.template/HeaderTemplate';
import OverlayTemplate from '@page-part/youtube-trot/component/4.template/OverlayTemplate';
import ItemsSearch from '@page-part/youtube-trot/component/item/ItemsSearch';
import { usePageRootStore } from '@page-part/youtube-trot/service/store/PageRootStoreConsumer';
import _ from 'lodash';
import { observer } from 'mobx-react';
import React, { useCallback, useEffect, useRef, useState } from 'react';
import sty from './SearchLayer.module.scss';

const SearchLayer = () => {
    const pageRootStore = usePageRootStore();
    const [searchResultItems, setSearchResultItems] = useState([]);
    const inputOnChangeHandler = (e) => {
        pageRootStore.ui.setSentence(e.target.value);
    };
    const refreshResultTiming = _.throttle((word) => {
        // 문장을 워드로 쪼개려면 split 하면되는데 알고리즘이 오래걸릴거 같아서 안해놨음
        setSearchResultItems(
            pageRootStore.play.list.getFilteredPlaylist({
                filter: (v) => {
                    return (
                        !!word &&
                        (v.id === word ||
                            v.singer.indexOf(word) > -1 ||
                            v.title.indexOf(word) > -1 ||
                            v.subTitle.indexOf(word) > -1 ||
                            v.hiddenKeyword.indexOf(word) > -1 ||
                            v.youtubeTitle.indexOf(word) > -1 ||
                            v.app.indexOf(word) > -1)
                    );
                },
            }),
        );
    }, 1000);
    const refreshResult = useCallback(refreshResultTiming, []);

    useEffect(() => {
        refreshResult(pageRootStore.ui.sentence);
    }, [pageRootStore.ui.sentence]);

    const wordClickHandler = (word: string) => {
        setTimeout(() => {
            pageRootStore.ui.setSentence(word);
        }, 500);
    };

    const inputSearchRef = useRef();
    useEffect(() => {
        if (!pageRootStore.ui.isSearchOpen) {
            setSearchResultItems([]);
            pageRootStore.ui.setSentence('');

            const event = new Event('focusout');
            (inputSearchRef.current as HTMLInputElement).dispatchEvent(event);
        } else {
            (inputSearchRef.current as HTMLInputElement).focus();
        }
    }, [pageRootStore.ui.isSearchOpen]);

    return (
        <OverlayTemplate isShow={pageRootStore.ui.isSearchOpen}>
            <div style={{ display: 'flex', flexDirection: 'column', height: '100%', position: 'relative' }}>
                <HeaderTemplate
                    left={
                        <BtnFullHeight
                            onClick={() => {
                                pageRootStore.play.link.clearSelected();
                                if (pageRootStore.ui.sentence) {
                                    pageRootStore.ui.setSentence('');
                                    return;
                                }
                                pageRootStore.ui.toggleSearch();
                            }}
                            text="뒤로가기"
                            style={{ width: '100%' }}
                        />
                    }
                    center="검색하세요 :)"
                />

                <div>
                    <input
                        ref={inputSearchRef}
                        className={sty.inputSearch}
                        type="text"
                        placeholder="무엇을 찾으세요 :?)"
                        value={pageRootStore.ui.sentence}
                        onChange={inputOnChangeHandler}
                    />
                </div>

                <div className={sty.wordsWrap}>
                    {!searchResultItems.length && (
                        <div className={sty.words}>
                            {pageRootStore.play.list.srcCategorySortedWords.map(([k, v]) => (
                                <div
                                    className={sty.word}
                                    key={k}
                                    onClick={() => {
                                        wordClickHandler(k);
                                    }}
                                >
                                    {k}
                                </div>
                            ))}
                        </div>
                    )}
                </div>

                {!!searchResultItems.length && <ItemsSearch items={searchResultItems} />}

                <div>{!!searchResultItems.length && <SelectController allItems={searchResultItems} />}</div>
            </div>
        </OverlayTemplate>
    );
};

export default observer(SearchLayer);
