import FooterTemplate from '@page-part/youtube-trot/component/4.template/FooterTemplate';
import { usePageRootStore } from '@page-part/youtube-trot/service/store/PageRootStoreConsumer';
import { observer } from 'mobx-react';
import React from 'react';
import * as Icon from '../../1.atom/svg';
import sty from './VideoFooter.module.scss';

const VideoFooter = () => {
    const pageRootStore = usePageRootStore();
    return (
        <FooterTemplate className={sty.main}>
            <div style={{ opacity: 0 }}>. . . . .</div>
            <div>
                <Icon.IconSkipPrev
                    className={sty.iconM}
                    onClick={() => {
                        pageRootStore.play.er.playPrev();
                    }}
                />
            </div>
            <div>
                {pageRootStore.play.er.state === 1 && pageRootStore.play.er.initPlay && (
                    <Icon.IconPause
                        className={sty.iconL}
                        onClick={() => {
                            pageRootStore.play.er.playToggle();
                        }}
                    />
                )}
                {pageRootStore.play.er.state !== 1 && pageRootStore.play.er.initPlay && (
                    <Icon.IconPlay
                        className={sty.iconL}
                        onClick={() => {
                            pageRootStore.play.er.playToggle();
                        }}
                    />
                )}
            </div>
            <div>
                <Icon.IconSkipNext
                    className={sty.iconM}
                    onClick={() => {
                        pageRootStore.play.er.playNext();
                    }}
                />
            </div>
            <div style={{ opacity: 0 }}>. . . . .</div>
        </FooterTemplate>
    );
};

export default observer(VideoFooter);
