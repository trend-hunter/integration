import BtnFullHeight from '@page-part/youtube-trot/component/1.atom/button/BtnFullHeight';
import HeaderTemplate from '@page-part/youtube-trot/component/4.template/HeaderTemplate';
import OverlayTemplate from '@page-part/youtube-trot/component/4.template/OverlayTemplate';
import ItemsPlaying from '@page-part/youtube-trot/component/item/ItemsPlaying';
import VideoLayerController from '@page-part/youtube-trot/component/layer/video/VideoFooter';
import { usePageRootStore } from '@page-part/youtube-trot/service/store/PageRootStoreConsumer';
import { observer } from 'mobx-react';
import React, { useEffect, useRef, useState } from 'react';
import PlayerFactory from 'youtube-player';
import NumberUtil from '@service/NumberUtil';
import { currStore } from '@page-part/youtube-trot/service/store/PageRootStore';
import WindowCustomContext from '@page-part/youtube-trot/service/WindowCustomContext';
import sty from './VideoLayer.module.scss';

/*
todo iframe -> youtube api로 변경
    isShow false일때 일시정지로 변경
 */
const VideoLayer = () => {
    const pageRootStore = usePageRootStore();
    const wrapRef = useRef();
    const [prevCurrId, setPrevCurrId] = useState('');

    useEffect(() => {
        if (window !== undefined) {
            const controller = PlayerFactory('player');
            pageRootStore.play.setErController(controller);

            const wrapWidth = (wrapRef.current as HTMLElement).offsetWidth || window.outerWidth;
            const size = NumberUtil.get16of9FullSizeBy100vw(wrapWidth);
            pageRootStore.play.er.controller.setSize(size.width, size.height);
            pageRootStore.play.er.controller.on('stateChange', (e) => {
                /*
                 -1 –시작되지 않음
                 0 – 종료
                 1 – 재생 중
                 2 – 일시중지
                 3 – 버퍼링
                 5 – 동영상 신호
                 */
                // eslint-disable-next-line default-case
                switch (e.data) {
                    case -1:
                        break;
                    case 0:
                        pageRootStore.play.er.onEnd();
                        break;
                    case 1:
                        pageRootStore.play.er.onPlay();
                        console.log(`< on play >`);
                        break;
                    case 2:
                        pageRootStore.play.er.onPause();
                        break;
                }
            });

            /*
            - `2` – 요청에 잘못된 매개변수 값이 포함되어 있습니다. 예를 들어 11자리가 아닌 동영상 ID를 지정하거나 동영상 ID에 느낌표 또는 별표와 같은 잘못된 문자가 포함된 경우에 이 오류가 발생합니다.
            - `5` – 요청한 콘텐츠를 HTML5 플레이어에서 재생할 수 없거나 HTML5 플레이어와 관련된 다른 오류가 발생했습니다.
            - `100` – 요청한 동영상을 찾을 수 없습니다. 어떠한 이유로든 동영상이 삭제되었거나 비공개로 표시된 경우에 이 오류가 발생합니다.
            - `101` – 요청한 동영상의 소유자가 내장 플레이어에서 동영상을 재생하는 것을 허용하지 않습니다.
            - `150` – 이 오류는 `101`과 동일하며 변형된 `101` 오류입니다.
            */
            //todo 여기에서 리포트 발송
            pageRootStore.play.er.controller.on('error', (e) => {
                console.log(`< event error >`, e);
            });

            WindowCustomContext.setPlayFunc({
                playPauseFunc: pageRootStore.play.er.pause,
                playResumeFunc: () => {
                    if (pageRootStore.ui.isPlayerOpen) {
                        pageRootStore.play.er.resume();
                    }
                },
            });
        }
        return () => {
            pageRootStore.play.setErController(null);
        };
    }, []);

    useEffect(() => {
        if (pageRootStore.ui.isPlayerOpen && window !== undefined && pageRootStore?.play?.er?.controller) {
            if (prevCurrId === pageRootStore.play.link.currId) {
                pageRootStore.play.er.resume();
            } else {
                pageRootStore.play.er.play();
                setPrevCurrId(pageRootStore.play.link.currId);
            }
        } else {
            currStore.play.er.pause().catch(() => {
                currStore.play.er.stop();
            });
        }
    }, [pageRootStore.ui.isPlayerOpen, pageRootStore.play.link.currId]);

    return (
        <OverlayTemplate isShow={pageRootStore.ui.isPlayerOpen} selfRef={wrapRef} style={{ overflow: 'hidden' }}>
            <div style={{ display: 'flex', flexDirection: 'column', height: '100%', position: 'relative' }}>
                <HeaderTemplate
                    left={
                        <BtnFullHeight
                            onClick={() => {
                                pageRootStore.ui.togglePlayer(false);
                            }}
                            text="뒤로가기"
                            style={{ width: '100%' }}
                        />
                    }
                    center={pageRootStore.play.list.getCurr()?.title}
                />
                <div
                    className={sty.main}
                    style={{
                        flex: '1',
                        display: 'flex',
                        flexDirection: 'column',
                        height: '100%',
                        position: 'relative',
                        overflow: 'hidden',
                    }}
                >
                    <div className={sty.youtube}>
                        <div id="player" />
                    </div>
                    <div style={{ flex: '1', overflowY: 'auto' }}>
                        <ItemsPlaying isDrag={false} />
                    </div>
                </div>
                <div>{pageRootStore.play.er.initPlay && <VideoLayerController />}</div>
            </div>
        </OverlayTemplate>
    );
};

export default observer(VideoLayer);
