import React, { useState } from 'react';
import PortalTemplate from '@page-part/youtube-trot/component/4.template/PortalTemplate';
import BtnFullHeight from '@page-part/youtube-trot/component/1.atom/button/BtnFullHeight';
import { Checkbox, FormControlLabel } from '@material-ui/core';
import { usePageRootStore } from '@page-part/youtube-trot/service/store/PageRootStoreConsumer';
import { observer } from 'mobx-react';
import sty from './VideoPolicyPortal.module.scss';
import { FunctionReturnVoid } from '../../../../Types';

interface Prop {
    okCallback: FunctionReturnVoid;
    cancelCallback: FunctionReturnVoid;
}

const VideoPolicyPortal = ({ okCallback, cancelCallback }: Prop) => {
    const [isChecked, setIsChecked] = useState(false);
    const pageRootStore = usePageRootStore();

    return (
        <>
            <PortalTemplate
                style={{ padding: 0 }}
                closeHandler={() => {
                    cancelCallback();
                }}
            >
                <div className={sty.content}>
                    <div className={sty.h1}>이 앱은 유튜브 영상 재생 정책을 따릅니다.</div>
                    <div className={sty.h2}>
                        화면이 꺼지거나 재생영상이 화면에서 사라질 경우 재생이 불가한 점 양해 부탁드립니다.
                    </div>
                    <div className={sty.wrapCheck}>
                        <FormControlLabel
                            control={
                                <Checkbox
                                    color="secondary"
                                    checked={isChecked}
                                    onChange={(event, checked) => {
                                        setIsChecked(checked);
                                    }}
                                />
                            }
                            label={<span className={sty.label}>다시 보지 않기</span>}
                        />
                    </div>
                </div>
                <BtnFullHeight
                    cssClass={sty.BtnPlay}
                    onClick={() => {
                        if (isChecked) {
                            pageRootStore.ui.portal.setTrueIsVideoPolicyPortalDisable();
                        }
                        okCallback();
                        cancelCallback();
                    }}
                    text="재생하기"
                />
            </PortalTemplate>
        </>
    );
};

export default observer(VideoPolicyPortal);
