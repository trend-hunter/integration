import React from 'react';

const OverflowHidden = () => {
    return (
        <style global jsx>{`
            body {
                overflow: hidden;
            }
        `}</style>
    );
};

export default OverflowHidden;
