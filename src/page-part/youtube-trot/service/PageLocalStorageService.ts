import PageSearchQuery from '@page-part/youtube-trot/service/PageSearchQuery';

const getKey = (): string => {
    return `key${PageSearchQuery.app}`;
};

export const getLinkKey = (): string => {
    return `${getKey()}-Link`;
};

export const getUiPortalKey = (): string => {
    return `${getKey()}-UiPortal`;
};

export default {
    getLinkKey,
    getUiPortalKey,
};
