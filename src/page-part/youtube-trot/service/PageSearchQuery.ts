class PageSearchQuery {
    private readonly urlSearchParams: URLSearchParams;

    public readonly app: string[] = ['미스터트롯'];

    constructor() {
        try {
            if (typeof window !== 'undefined') {
                this.urlSearchParams = new URLSearchParams(location.search);
                this.app = this.urlSearchParams.get('app')
                    ? decodeURIComponent(this.urlSearchParams.get('app')).split(',')
                    : ['미스터트롯'];
            }
        } catch (e) {
            throw new Error(e);
        }
    }
}

export default new PageSearchQuery();
