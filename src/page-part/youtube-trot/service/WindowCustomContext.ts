const TREND_HUNTER = {
    // eslint-disable-next-line @typescript-eslint/no-empty-function
    playPauseFunc: () => {},
    // eslint-disable-next-line @typescript-eslint/no-empty-function
    playResumeFunc: () => {},
};

if (typeof window !== 'undefined') {
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    window.TREND_HUNTER = TREND_HUNTER;
}

const setPlayFunc = ({ playPauseFunc, playResumeFunc }) => {
    TREND_HUNTER.playPauseFunc = playPauseFunc;
    TREND_HUNTER.playResumeFunc = playResumeFunc;
};

export default {
    setPlayFunc,
};
