import { Transform } from 'class-transformer';
import { IsArray, IsBoolean, IsNotEmpty, IsNotIn, IsString } from 'class-validator';

export default class ItemModel {
    @Transform((value) => {
        if (Array.isArray(value)) {
            return [...value];
        }
        return [value];
    })
    @IsArray()
    @IsNotEmpty()
    @IsNotIn(['N/A'])
    app: string[];

    @Transform((value) => String(value))
    @IsString()
    @IsNotEmpty()
    id: string;

    @IsString()
    @IsNotEmpty()
    @IsNotIn(['N/A'])
    thumbnail: string;

    @IsString()
    @IsNotEmpty()
    @IsNotIn(['N/A', ''])
    youtubeId: string;

    @IsString()
    @IsNotEmpty()
    // @IsNotIn(['N/A'])
    youtubeTitle: string;

    @Transform((value) => String(value))
    @IsString()
    @IsNotEmpty()
    @IsNotIn(['N/A'])
    title: string;

    @Transform((value) => {
        if (Array.isArray(value)) {
            return [...value];
        }
        return [value];
    })
    @IsArray()
    @IsNotEmpty()
    @IsNotIn(['N/A'])
    singer: string[];

    @Transform((value = []) => {
        if (Array.isArray(value)) {
            return [...value];
        }
        return [value];
    })
    hiddenKeyword: string[] = [];

    @IsString()
    @IsNotEmpty()
    @IsNotIn(['N/A'])
    subTitle: string;

    @Transform((value) => String(value).toLowerCase() === 'y')
    @IsBoolean()
    @IsNotEmpty()
    active: boolean;
}

// export default {};
