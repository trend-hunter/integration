import ItemModel from '@page-part/youtube-trot/service/model/ItemModel';
import { transformAndValidate } from 'class-transformer-validator';
import mock from '../mock/mock.json';

export async function fetch(): Promise<ItemModel[]> {
    // eslint-disable-next-line no-return-await,@typescript-eslint/ban-ts-comment
    // @ts-ignore
    // eslint-disable-next-line no-return-await
    return await (mock as ItemModel[]).reduce(async (accumP, current, index) => {
        const accum = await accumP;
        transformAndValidate(ItemModel, current).then(
            (v) => {
                if (String(current.active).toLowerCase() === 'y') {
                    accum.push(v);
                }
            },
            (v) => {
                if (String(current.active).toLowerCase() === 'y') {
                    console.log(current.active, 'fetch v2 error', v, current);
                }
            },
        );
        return accum;
    }, Promise.resolve([]));
}
export default {
    fetch,
};
