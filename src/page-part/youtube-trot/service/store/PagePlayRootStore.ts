import { flow, addMiddleware, getRoot, Instance, types } from 'mobx-state-tree';
import PagePlayLinkStore from '@page-part/youtube-trot/service/store/play/PagePlayLinkStore';
import PagePlayerStore from '@page-part/youtube-trot/service/store/play/PagePlayerStore';
import PagePlayListStore from '@page-part/youtube-trot/service/store/play/PagePlayListStore';
import { YouTubePlayer } from 'youtube-player/dist/types';

const PagePlayRootStore = types
    .model({
        link: types.optional(PagePlayLinkStore, {}),
        er: types.optional(PagePlayerStore, { seek: 0, state: -1 }),
        list: types.optional(PagePlayListStore, {}),
    })
    .actions((self) => {
        return {
            setErController: (controller: YouTubePlayer) => {
                self.er.controller = controller;
            },
        };
    })
    .views((self) => ({}));
export default PagePlayRootStore;
export type IPagePlayRootStore = Instance<typeof PagePlayRootStore>;
