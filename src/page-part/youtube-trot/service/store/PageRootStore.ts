import PagePlayLinkStore from '@page-part/youtube-trot/service/store/play/PagePlayLinkStore';
import PageUiRootStore, { PortalStore } from '@page-part/youtube-trot/service/store/PageUIRootStore';
import makeInspectable from 'mobx-devtools-mst';
import { addMiddleware, applySnapshot, Instance, onSnapshot, types } from 'mobx-state-tree';
import PagePlayRootStore from '@page-part/youtube-trot/service/store/PagePlayRootStore';
import PageLocalStorageService from '@page-part/youtube-trot/service/PageLocalStorageService';

export const PageRootStore = types
    .model({
        ui: types.optional(PageUiRootStore, {}),
        play: types.optional(PagePlayRootStore, {}),
    })
    .actions((self) => ({
        //
    }))
    .views((self) => ({
        //
    }));

export type IPageRootStore = Instance<typeof PageRootStore>;
// eslint-disable-next-line import/no-mutable-exports
export let currStore = PageRootStore.create();

onSnapshot(currStore.play.link, (snapshot) => {
    localStorage.setItem(PageLocalStorageService.getLinkKey(), JSON.stringify(snapshot));
});
onSnapshot(currStore.ui.portal, (snapshot) => {
    localStorage.setItem(PageLocalStorageService.getUiPortalKey(), JSON.stringify(snapshot));
});

addMiddleware(currStore.play, (actionCall, next, abort) => {
    if (actionCall.name === 'clearIngIdMap') {
        currStore.ui.togglePlayer(false);
        currStore.ui.toggleSearch(false);
        currStore.ui.setMainFragmentCurrIndex(0);
    } else if (actionCall.name === 'removeIngId') {
        if (currStore.play.link.ingIdMap.size === 1) {
            currStore.ui.togglePlayer(false);
            currStore.ui.toggleSearch(false);
            currStore.ui.setMainFragmentCurrIndex(0);
        }
    }
    return next(actionCall);
});

function applySnapshotFromLocalStorageLink(snapshot) {
    if (PagePlayLinkStore.is(snapshot)) {
        applySnapshot(currStore.play.link, snapshot);
    } else {
        localStorage.clear();
        throw new Error(snapshot);
    }
}
function applySnapshotFromLocalStorageUiPortal(snapshot) {
    if (PortalStore.is(snapshot)) {
        applySnapshot(currStore.ui.portal, snapshot);
    } else {
        localStorage.clear();
        throw new Error(snapshot);
    }
}

export function initializeStore({ link, uiPortal }: { link?: JSON; uiPortal?: JSON }) {
    const storeInstance = currStore ?? PageRootStore.create();
    makeInspectable(currStore);

    if (typeof window === undefined) {
        return storeInstance;
    }
    if (!currStore) {
        currStore = storeInstance;
    }
    if (link) {
        applySnapshotFromLocalStorageLink(link);
    }
    if (uiPortal) {
        applySnapshotFromLocalStorageUiPortal(uiPortal);
    }

    return currStore;
}
