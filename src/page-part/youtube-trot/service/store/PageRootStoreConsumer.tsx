import React, { createContext, useContext } from 'react';
import { initializeStore, IPageRootStore } from './PageRootStore';

const PageRootStoreContext = createContext<IPageRootStore | null>(null);
export const PageRootStoreConsumer = PageRootStoreContext.Provider;

export function usePageRootStore(): IPageRootStore {
    const store = useContext(PageRootStoreContext);
    if (store === null) {
        throw new Error('Store cannot be null, please add a context provider');
    }
    return store;
}

interface Props {
    children?: JSX.Element[] | JSX.Element;
    snapshot?: JSON | string;
}

const Consumer = ({ children }: Props) => {
    const store = initializeStore({});
    return <PageRootStoreConsumer value={store}>{children}</PageRootStoreConsumer>;
};

export default Consumer;
