import { types } from 'mobx-state-tree';

export const PortalStore = types
    .model({
        isShowVideoPolicyPortalDisable: types.optional(types.boolean, false),
    })
    .actions((self) => ({
        setTrueIsVideoPolicyPortalDisable: () => {
            self.isShowVideoPolicyPortalDisable = true;
        },
    }));

const PageUiRootStore = types
    .model('PageUiRootStore', {
        isPlayerOpen: types.optional(types.boolean, false),
        isSearchOpen: types.optional(types.boolean, false),
        mainFragmentCurrIndex: types.optional(types.number, 0),
        sentence: types.optional(types.string, ''),
        portal: types.optional(PortalStore, { isShowVideoPolicyPortalDisable: false }),
    })
    .actions((self) => ({
        setMainFragmentCurrIndex: (i: number) => {
            self.mainFragmentCurrIndex = i;
        },
        setSentence: (s: string) => {
            self.sentence = s;
        },
        toggleSearch: (force?: boolean) => {
            if (force !== undefined) {
                self.isSearchOpen = force;
                return;
            }
            self.isSearchOpen = !self.isSearchOpen;
        },
        togglePlayer: (force?: boolean) => {
            if (force !== undefined) {
                self.isPlayerOpen = force;
                return;
            }
            self.isPlayerOpen = !self.isPlayerOpen;
        },
    }))
    .views((self) => ({
        //
    }));

export default PageUiRootStore;
