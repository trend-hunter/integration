import ItemModel from '@page-part/youtube-trot/service/model/ItemModel';
import { IPageRootStore } from '@page-part/youtube-trot/service/store/PageRootStore';
import { getRoot, types } from 'mobx-state-tree';

const SettingStore = types
    .model({
        isRandom: types.boolean,
        /**
         * 1: all loop
         * 0: 1 loop
         * -1: no loop
         */
        loopType: types.number,
    })
    .actions((self) => ({
        toggleRandom: () => {
            self.isRandom = !self.isRandom;
        },
        toggleLoop: () => {
            const curr = self.loopType;
            self.loopType = curr === -1 ? 1 : curr - 1;
        },
    }));

const PagePlayLinkStore = types
    .model('PagePlayLinkStore', {
        currId: types.optional(types.string, ''),
        /**
         * k: id
         * v: timestamp
         */
        ingIdMap: types.map(types.number),
        selectedIdMap: types.map(types.number),
        setting: types.optional(SettingStore, { isRandom: false, loopType: 1 }),
        /**
         * 랜덤, 안랜덤
         * 뒤로 // 랜덤과 연관
         * 재생, 일시정지 // 변수 필요없음
         * 앞으로 // 랜덤과 연관
         * 한곡반복, 반복없음, 전체재생 // pure 한 기능의 앞뒤가 이벤트로 동작
         */
    })
    .actions((self) => ({
        randomCurr: (): void => {
            const root = getRoot<IPageRootStore>(self);
            const arr = root.play.link.sortedIngIdList;
            const currIndex = arr.findIndex((value) => value === root.play.link.currId);
            const random = Math.floor(Math.random() * arr.length);

            //random 결과가 현재와 같거나 현재다음곡이라면 재귀
            if (random === currIndex) {
                root.play.link.nextCurr();
                root.play.link.nextCurr();
            } else {
                self.currId = arr[random];
            }
        },
        nextCurr: (): void => {
            const root = getRoot<IPageRootStore>(self);
            const arr = root.play.link.sortedIngIdList;
            const i = arr.findIndex((value) => value === self.currId);
            if (i < arr.length - 1 && i !== -1) {
                self.currId = arr[i + 1];
            } else {
                // eslint-disable-next-line prefer-destructuring
                self.currId = arr[0];
            }
        },
        prevCurr: (): void => {
            // eslint-disable-next-line @typescript-eslint/ban-ts-comment
            // @ts-ignore
            const root = getRoot<IPageRootStore>(self);
            const arr = root.play.link.sortedIngIdList;
            const i = arr.findIndex((value) => value === self.currId);
            if (i > 0) {
                self.currId = arr[i - 1];
            } else {
                self.currId = arr[arr.length - 1];
            }
        },
        clearSelected: (): void => {
            self.selectedIdMap.clear();
        },
        clearIngIdMap: (): void => {
            self.ingIdMap.clear();
        },
        switchPositionIngMap: (index1, index2): void => {
            if (index1 === index2) {
                return;
            }
            const root = getRoot<IPageRootStore>(self);
            const id1 = Array.from(root.play.link.sortedIngIdList).find((id, index) => {
                return index === index1;
            });
            const id2 = Array.from(root.play.link.sortedIngIdList).find((id, index) => {
                return index === index2;
            });
            if (!id1 || !id2) {
                return;
            }
            const item1 = root.play.list.src.get(id1);
            const item2 = root.play.list.src.get(id2);
            if (!item1 || !item2) {
                return;
            }
            const padding = -index1 + index2 > 0 ? -1 : 1;
            // console.log(`< 1 >`, isDown, id1, item1.title.substring(0, 5), id2, item2.title.substring(0, 5));

            Array.from(self.ingIdMap.entries())
                .sort((a, b) => a[1] - b[1])
                .forEach((value, index) => {
                    self.ingIdMap.set(value[0], index * 10);
                });

            const nv = root.play.link.ingIdMap.get(id2) + padding;
            root.play.link.ingIdMap.set(id1, nv);
        },
        removeIngId: (model: ItemModel): void => {
            self.ingIdMap.delete(model.id);
            if (model.id === self.currId) {
                const root = getRoot<IPageRootStore>(self);
                root.play.link.nextCurr();
            }
        },
        toggleSelected: (model: ItemModel): void => {
            // eslint-disable-next-line no-unused-expressions
            self.selectedIdMap.has(model.id)
                ? self.selectedIdMap.delete(model.id)
                : self.selectedIdMap.set(model.id, new Date().getTime());
        },
        putSelectedToIng: (): string => {
            // const ingMapLength = self.ingIdMap.size;
            const entry = Array.from(self.selectedIdMap.entries());
            entry.forEach(([id, timestamp], index, array) => {
                self.ingIdMap.set(id, timestamp);
            });
            self.selectedIdMap.clear();
            return entry[entry.length - 1][0];
        },
        putAllToIng: (ids: string[] = []): void => {
            const time = new Date().getTime();
            let cnt = 1;
            ids.forEach((k) => {
                if (!self.ingIdMap.has(k)) {
                    self.ingIdMap.set(k, time + cnt);
                    cnt += 1;
                }
            });
            const root = getRoot<IPageRootStore>(self);
            root.play.link.setCurrId(ids[ids.length - 1]);
        },
        setCurrId: (id: string): void => {
            self.currId = id;
        },
    }))
    .views((self) => ({
        get sortedIngIdList(): string[] {
            // eslint-disable-next-line @typescript-eslint/ban-ts-comment
            // @ts-ignore
            return (
                Array.from(self.ingIdMap.entries())
                    //추가된 시간 순서의 역순으로 정렬
                    .sort((a, b) => -a[1] + b[1])
                    .map((value) => value[0])
            );
        },
        get isLastCurr(): boolean {
            const root = getRoot<IPageRootStore>(self);
            const i = this.sortedIngIdList.findIndex((v) => v === root.play.link.currId);
            return !!(i === -1 || i === this.sortedIngIdList.length - 1);
        },
    }));

// export hiddenKeyword IPagePlayStore = Instance<typeof PagePlayLinkStore>;
export default PagePlayLinkStore;
