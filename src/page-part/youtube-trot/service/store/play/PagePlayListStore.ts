import { flow, getRoot, IStateTreeNode, IType, types } from 'mobx-state-tree';
import ItemModel from '@page-part/youtube-trot/service/model/ItemModel';
import PagePlayListRepository from '@page-part/youtube-trot/service/repo/PlayListRepository';
import { IPageRootStore } from '@page-part/youtube-trot/service/store/PageRootStore';

const app = types
    .model({
        //todo 이 셋트로 각각에 필요한 store 만들어서 파자
        //  필요한 스토어는 | app(tabs), search, playing
        testFlag: types.optional(types.number, 0),
        testSrc: types.array(types.frozen<ItemModel>()),
        testSrcLoading: types.optional(types.boolean, false),
    })
    .actions((self) => ({
        // toggleRandom: () => {
        //     self.isRandom = !self.isRandom;
        // },
        // toggleLoop: () => {
        //     const curr = self.loopType;
        //     self.loopType = curr === -1 ? 1 : curr - 1;
        // },
    }));

const ing = types
    .model({
        //todo 이 셋트로 각각에 필요한 store 만들어서 파자
        //  필요한 스토어는 | app(tabs), search, playing
        testFlag: types.optional(types.number, 0),
        testSrc: types.array(types.frozen<ItemModel>()),
        testSrcLoading: types.optional(types.boolean, false),
    })
    .actions((self) => ({
        // toggleRandom: () => {
        //     self.isRandom = !self.isRandom;
        // },
        // toggleLoop: () => {
        //     const curr = self.loopType;
        //     self.loopType = curr === -1 ? 1 : curr - 1;
        // },
    }));

const tab = types
    .model({
        //todo 이 셋트로 각각에 필요한 store 만들어서 파자
        //  필요한 스토어는 | app(tabs), search, playing
        testFlag: types.optional(types.number, 0),
        testSrc: types.array(types.frozen<ItemModel>()),
        testSrcLoading: types.optional(types.boolean, false),
    })
    .actions((self) => ({
        // toggleRandom: () => {
        //     self.isRandom = !self.isRandom;
        // },
        // toggleLoop: () => {
        //     const curr = self.loopType;
        //     self.loopType = curr === -1 ? 1 : curr - 1;
        // },
    }));

// const search;

/*
main filter
tab filter
 */
const PagePlayListStore = types
    .model({
        //todo 이거 store로 뺄까? | reference로 하면 좋긴한데
        src: types.map(types.frozen<ItemModel>()),
        app: types.array(types.string),
        //todo 이 셋트로 각각에 필요한 store 만들어서 파자
        //  필요한 스토어는 | app(tabs), search, playing
        testFlag: types.optional(types.number, 0),
        testSrc: types.array(types.frozen<ItemModel>()),
        testSrcLoading: types.optional(types.boolean, false),
    })
    .actions((self) => {
        const root = getRoot<IPageRootStore>(self);

        return {
            //todo 하나만 실행되게 어떻게 하자?
            //  이거되면 아무리 복잡한거도 넣을 수 있을거 같다. 대신 로딩중이라는 표시는 해줘야겠지
            //  검색을 빼서 음 스토어로 만들면 좋을거 같은데 ui으로 옮길까?
            //  익런 카테고리에 바로 쓸 수 있을거 같은데
            /*
            todo 여기만 할게 아니라 전체재생, 선택 재생에 먼저 적용하자.
             */
            getTestSrc: flow(function* f() {
                // eslint-disable-next-line no-plusplus
                const flag = ++self.testFlag;
                self.testSrcLoading = true;
                root.play.list.clearTestSrc();

                // const valIter = Array.from(self.src.values());
                const valIter = self.src.values();

                // while (valIter.next().value) {}
                let node: IteratorResult<ItemModel & IStateTreeNode<IType<ItemModel, ItemModel, ItemModel>>, any>;
                // eslint-disable-next-line no-cond-assign
                while ((node = valIter.next()) && !node.done) {
                    console.log(`< item >`, node);
                    const item = node.value;

                    if (flag !== self.testFlag) {
                        root.play.list.clearTestSrc();
                        self.testSrcLoading = false;
                        return 0;
                    }

                    yield new Promise<ItemModel>((resolve, reject) => {
                        setTimeout(() => {
                            //todo 여기에서 add add add 해주면 observer 하는데서 알아서 넣어주겠지.
                            // console.log(`< promise >`, argumene.id);
                            // item.test();
                            // console.log(`< arg >`, argumene);
                            root.play.list.pushTestSrc(item);
                            resolve();
                        }, 1000);
                    });
                }

                self.testSrcLoading = false;
                return 1;
            }),
            pushTestSrc: (v: ItemModel) => {
                self.testSrc.push(v);
            },
            clearTestSrc: () => {
                self.testSrc.clear();
            },
            addApp: (category: string | string[]): void => {
                if (typeof category === 'string') {
                    self.app.push(category);
                } else if (Array.isArray(category)) {
                    category.forEach((value) => self.app.push(value));
                }
            },
            load: flow(function* f() {
                const resultFetch: ItemModel[] = yield PagePlayListRepository.fetch();
                resultFetch.forEach((value) => {
                    self.src.set(value.id, value);
                });
                return self.src;
            }),
            getCurr: (): ItemModel => {
                const root = getRoot<IPageRootStore>(self);
                return Array.from(self.src.values()).find((v) => v.id === root.play.link.currId);
            },
            getFilteredPlaylist: ({ filter }: { filter: (v: ItemModel) => boolean }): ItemModel[] => {
                //todo 제너레이터로 변경필요
                return Array.from(self.src.values()).filter(filter);
            },
            getFilteredPlaylistaa: function* f({
                filter,
            }: {
                filter: (v: ItemModel) => boolean;
            }): Generator<ItemModel, void, unknown> {
                const valIter = self.src.values();
                let node: IteratorResult<ItemModel & IStateTreeNode<IType<ItemModel, ItemModel, ItemModel>>, any>;
                // eslint-disable-next-line no-cond-assign
                while ((node = valIter.next()) && !node.done) {
                    // const item = node.value;
                    yield node.value;

                    // eslint-disable-next-line no-loop-func
                    // yield new Promise<ItemModel>((resolve, reject) => {
                    //     setTimeout(() => {
                    //         //todo 여기에서 add add add 해주면 observer 하는데서 알아서 넣어주겠지.
                    //         // console.log(`< promise >`, argumene.id);
                    //         // item.test();
                    //         // console.log(`< arg >`, argumene);
                    //         // root.play.list.pushTestSrc(item)
                    //         resolve(node.value);
                    //     }, 1000);
                    // });

                    // setTimeout(() => {
                    //     yield node.value;
                    // });

                    // yield new Promise<ItemModel>((resolve, reject) => {
                    //     setTimeout(() => {
                    //         //todo 여기에서 add add add 해주면 observer 하는데서 알아서 넣어주겠지.
                    //         // console.log(`< promise >`, argumene.id);
                    //         // item.test();
                    //         // console.log(`< arg >`, argumene);
                    //         // root.play.list.pushTestSrc(item)
                    //         resolve();
                    //     }, 1000);
                    // });
                }
            },
        };
    })
    .views((self) => {
        const root = getRoot<IPageRootStore>(self);

        return {
            get srcCategoryValues(): ItemModel[] {
                if (!self.app.length) {
                    return root.play.list.srcValues;
                }
                // const srcCateVal = root.play.list.srcValues.filter(
                //     (value) => value.app.indexOf(self.app) > -1,
                // );

                const copied = Array.from(root.play.list.srcValues);
                let srcCateVal = [];

                // todo 개선할 수 있음
                // eslint-disable-next-line no-restricted-syntax
                for (const cate of self.app) {
                    srcCateVal = srcCateVal.concat(
                        copied.reduce((previousValue, currentValue, currentIndex) => {
                            if (currentValue && currentValue.app.indexOf(cate) > -1) {
                                previousValue.push(currentValue);
                                copied[currentIndex] = null;
                            }
                            return previousValue;
                        }, []),
                    );
                }

                if (!srcCateVal.length) {
                    return root.play.list.srcValues;
                }
                return srcCateVal;
            },
            get srcCategoryTabs(): { tabTitle: string[]; tabPlaylist: ItemModel[][] } {
                const map = new Map<string, number>();
                root.play.list.srcCategoryValues
                    //todo 임시
                    .map((value) => value.hiddenKeyword[0])
                    .forEach((value) => {
                        if (map.has(value)) {
                            map.set(value, map.get(value) + 1);
                        } else {
                            map.set(value, 0);
                        }
                    });
                const tabTitle = Array.from(map.entries())
                    .sort((a, b) => -a[1] + b[1])
                    .map((value) => value[0]);
                const tabPlaylist = tabTitle.reduce((previousValue, currentValue) => {
                    previousValue.push(
                        //todo 임시
                        root.play.list.srcCategoryValues.filter((value) => value.hiddenKeyword[0] === currentValue),
                    );
                    return previousValue;
                }, []);
                return { tabTitle, tabPlaylist };
            },
            get srcKeys(): string[] {
                return Array.from(self.src.keys());
            },
            get srcValues(): ItemModel[] {
                return Array.from(self.src.values());
            },
            get srcCategorySortedWords() {
                const map = new Map();
                root.play.list.srcCategoryValues
                    .flatMap((value) => [...value.singer, value.title, value.subTitle])
                    .forEach((value) => {
                        if (map.has(value)) {
                            map.set(value, map.get(value) + 1);
                        } else {
                            map.set(value, 0);
                        }
                    });

                return Array.from(map.entries())
                    .sort((a, b) => -a[1] + b[1])
                    .filter((value) => value[1] > 0);
            },
            get ingList(): ItemModel[] {
                const root = getRoot<IPageRootStore>(self);
                const sortedIngId = root.play.link.sortedIngIdList;
                return sortedIngId.reduce((previousValue, currentValue) => {
                    if (self.src.has(currentValue)) {
                        previousValue.push(self.src.get(currentValue));
                    }
                    return previousValue;
                }, []);
            },
            // get aaaaa(): Generator<any, void, unknown> {
            //     const root = getRoot<IPageRootStore>(self);
            //     const sortedIngId = root.play.link.sortedIngIdList;
            //
            //     return (function* f() {
            //         for (let i = 0; i < sortedIngId.length; i++) {
            //             if (self.src.has(sortedIngId[i])) {
            //                 yield self.src.get(sortedIngId[i]);
            //             }
            //         }
            //     })();
            // },
        };
    });
export default PagePlayListStore;
