/*
 
 -1 –시작되지 않음
 0 – 종료
 1 – 재생 중
 2 – 일시중지
 3 – 버퍼링
 5 – 동영상 신호

 */
import { flow, getRoot, types } from 'mobx-state-tree';
import { YouTubePlayer } from 'youtube-player/dist/types';
import _ from 'lodash';
import { IPageRootStore } from '@page-part/youtube-trot/service/store/PageRootStore';

const PagePlayerStore = types
    .model({
        controller: types.optional(types.frozen<YouTubePlayer>(), null),
        //
        seek: types.number,
        state: types.number,
        initPlay: types.optional(types.boolean, false),
    })
    .actions((self) => {
        const playDebounce = _.debounce((rootStore: IPageRootStore) => {
            if (rootStore.play.list?.getCurr()?.youtubeId && rootStore.ui?.isPlayerOpen) {
                rootStore.play.er.controller.loadVideoById(rootStore.play.list.getCurr().youtubeId);
                // const size = NumberUtil.get16of9FullSizeBy100vw(window.outerWidth);
                // rootStore.play.er.controller.setSize(size.width, size.height);
                rootStore.play.er.controller.playVideo();
                rootStore.play.er.setSeek(0);
                rootStore.play.er.setState(1);
            }
        }, 400);

        return {
            setSeek: (n: number) => {
                self.seek = n;
            },
            setState: (n: number) => {
                self.state = n;
            },
            play: () => {
                const root = getRoot<IPageRootStore>(self);
                playDebounce(root);
            },
            // eslint-disable-next-line @typescript-eslint/ban-ts-comment
            // @ts-ignore
            pause: flow(function* f() {
                // eslint-disable-next-line @typescript-eslint/ban-ts-comment
                // @ts-ignore
                self.seek = yield self.controller.getCurrentTime();
                self.controller.pauseVideo();
                self.state = 2;
            }),
            resume: () => {
                self.controller.seekTo(self.seek, true);
                self.controller.playVideo();
                self.seek = 0;
                self.state = 1;
            },
            stop: () => {
                self.controller.stopVideo();
                self.seek = 0;
                self.state = 0;
            },
            rePlay: () => {
                self.controller.seekTo(0, true);
                self.controller.playVideo();
                self.seek = 0;
                self.state = 1;
            },
            // eslint-disable-next-line @typescript-eslint/ban-ts-comment
            // @ts-ignore
            playToggle: flow(function* f() {
                const root = getRoot<IPageRootStore>(self);
                const state = yield self.controller.getPlayerState();
                //재생중
                if (state === 1) {
                    root.play.er.pause().catch(() => {
                        root.play.er.stop();
                    });
                }
                //일시정지중
                else if (state === 2) {
                    root.play.er.resume();
                } else {
                    root.play.er.play();
                }
            }),
            playNext: () => {
                const root = getRoot<IPageRootStore>(self);
                root.play.link.nextCurr();
                root.play.er.play();
            },
            playPrev: () => {
                const root = getRoot<IPageRootStore>(self);
                root.play.link.prevCurr();
                root.play.er.play();
            },
            // eslint-disable-next-line @typescript-eslint/ban-ts-comment
            // @ts-ignore
            onPause: flow(function* f() {
                // eslint-disable-next-line @typescript-eslint/ban-ts-comment
                // @ts-ignore
                self.seek = yield self.controller.getCurrentTime();
                self.state = 2;
            }),
            onPlay: () => {
                self.state = 1;
                self.initPlay = true;
            },
            onEnd: () => {
                //todo 설정적용
                const root = getRoot<IPageRootStore>(self);
                if (root.play.link.setting.loopType === 0) {
                    root.play.er.rePlay();
                } else if (root.play.link.setting.loopType === -1) {
                    if (!root.play.link.isLastCurr) {
                        root.play.er.playNext();
                    }
                } else {
                    if (root.play.link.setting.isRandom) {
                        root.play.link.randomCurr();
                    } else {
                        root.play.link.nextCurr();
                    }
                    root.play.er.play();
                }
            },
        };
    })
    .views((self) => ({
        // get parent() {
        //     return getParent(self) as IPagePlayStore;
        // },
        // get root() {
        //     return getRoot(self);
        // },
    }));
export default PagePlayerStore;
