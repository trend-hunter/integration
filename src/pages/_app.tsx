import 'antd/dist/antd.min.css';
// eslint-disable-next-line import/no-extraneous-dependencies
import 'mobx-react-lite/batchingForReactDom';
import '../style/Global.scss';
import React from 'react';
import 'reflect-metadata'; //class-transformer 를 위해 필요한 선언
import '../style/Reset.scss';

export default function App({ Component, pageProps }) {
    //root로 부터 내려주면 자식들은 다른 store를 주입 못받는다
    // const store = useStore(pageProps.initialState);
    return (
        <>
            {/*<Head>*/}
            {/*    <title>trend hunter</title>*/}
            {/*    <meta httpEquiv="Content-Type" content="text/html; charset=utf-8" />*/}
            {/*    <meta name="viewport" content="initial-scale=1.0, width=device-width" />*/}
            {/*</Head>*/}
            <Component {...pageProps} />
        </>
    );
}

// App.getInitialProps = async (props) => {
//     const { Component, ctx } = props;
//     const { req } = ctx;
//     // Parses Next.js cookies in a universal way (server + client) - It's an object
//     const cookies = NextCookies(ctx);
//     // Universally detects the user's language
//     const lang = LanguageDetector({ cookies, req });
//
//     /*
//     Calls page's `getInitialProps` and fills `appProps.pageProps` - XXX
//     See https://nextjs.org/docs#custom-app
//     */
//     const appProps = await NextApp.getInitialProps(props);
//
//     appProps.pageProps = {
//         ...appProps.pageProps,
//         cookies, // Object containing all cookies
//         lang, // i.e: 'en'
//         isSSR: !!req,
//     };
//
//     return { ...appProps };
// };
