import Document, { Head, Main, NextScript } from 'next/document';
import NextCookies from 'next-cookies';
import React from 'react';
import { PortalRootId } from '../Const';

export default class MyDocument extends Document<any> {
    public static async getInitialProps(ctx) {
        const initialProps = await Document.getInitialProps(ctx);
        const cookies = NextCookies(ctx);
        return { ...initialProps };
    }

    public render() {
        const { lang } = this.props;
        return (
            <html lang={lang}>
                <Head>
                    <meta charSet="utf-8" />
                    <meta name="android:app" content="구글 계정 trend.investers@gmail.com로 서비스 되는 웹입니다." />
                    {/*<script*/}
                    {/*    data-ad-client="ca-pub-8287059551880588"*/}
                    {/*    async*/}
                    {/*    src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"*/}
                    {/*/>*/}
                </Head>
                <body>
                    <Main />
                    <NextScript />
                    <div id={PortalRootId} />
                </body>
            </html>
        );
    }
}
