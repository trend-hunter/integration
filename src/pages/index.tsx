import Page from '@page-part/youtube-trot/component/0.page/Page';
import PageRootStoreConsumer from '@page-part/youtube-trot/service/store/PageRootStoreConsumer';
import React from 'react';

// eslint-disable-next-line no-use-before-define
const Main = () => {
    return (
        <PageRootStoreConsumer>
            <Page />
        </PageRootStoreConsumer>
    );
};

export default Main;

export async function getStaticProps(ctx) {
    return { props: {} };
}

// export async function getServerSideProps(context) {
//     const snapshotId = context.query[SearchKeySet.SnapshotId];
//     const snapshotApiResultPromise: firebase.firestore.DocumentData = snapshotId
//         ? await SnapshotApi.get(snapshotId)
//         : null;
//     const snapshotApiResultObject = snapshotApiResultPromise ? snapshotApiResultPromise.data() : null;
//     return {
//         props: {
//             snapshotApiResult: JSON.parse(JSON.stringify(snapshotApiResultObject)),
//         },
//     };
// }

// export async function getStaticProps(context) {
//     return { props: {} };
// }
