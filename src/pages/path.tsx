import { getChildPaths } from '@static/DirectoryTree';
import Link from 'next/link';
import React from 'react';

export default function Page(props) {
    // eslint-disable-next-line react/destructuring-assignment
    const Nodes = props.pathList.map((str) => (
        <div key={str}>
            <Link href={str}>{str}</Link>
        </div>
    ));
    return <div>{Nodes}</div>;
}

export async function getStaticProps(ctx) {
    const pathList: string[] = getChildPaths();
    return { props: { pathList } };
}

// export async function getServerSideProps(context) {
//     const snapshotId = context.query[SearchKeySet.SnapshotId];
//     const snapshotApiResultPromise: firebase.firestore.DocumentData = snapshotId
//         ? await SnapshotApi.get(snapshotId)
//         : null;
//     const snapshotApiResultObject = snapshotApiResultPromise ? snapshotApiResultPromise.data() : null;
//     return {
//         props: {
//             snapshotApiResult: JSON.parse(JSON.stringify(snapshotApiResultObject)),
//         },
//     };
// }

// export async function getStaticProps(context) {
//     return { props: {} };
// }
