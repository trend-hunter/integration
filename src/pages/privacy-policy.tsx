import React from 'react';

const Page = () => {
    return (
        <div>
            <style global jsx>{`
                body {
                    background: black;
                    color: white;
                    max-width: 800px;
                    padding: 4rem 1rem;
                    margin: auto;
                }
                .h1 {
                    font-weight: bold;
                    margin: 3rem 0;
                    font-size: 1.3rem;
                }
                .h2 {
                    font-weight: bold;
                    margin: 2rem 0;
                    font-size: 1.2rem;
                }
                .h3 {
                    margin: 1rem 0;
                    font-size: 1rem;
                }
                .desc {
                }
            `}</style>
            <div className="body">
                <div>
                    This Policy (the &quot;Policy&quot;) explains the way of treatment of the information which is
                    provided or collected in the web sites on which this Policy is posted. In addition the Policy also
                    explains the information which is provided or collected in the course of using the applications of
                    the Company which exist in the websites or platforms of other company. The Company is the controller
                    of the information provided or collected in the websites on which this Policy is posted and in the
                    course of using the applications of the Company which exist in the websites or platforms of other
                    company.Through this Policy, the Company regards personal information of the users as important and
                    inform them of the purpose and method of Company&apos;s using the personal information provided by
                    the users and the measures taken by the Company for protection of those personal information. This
                    Policy will be effective on the 12th day of June, 2020 and, in case of modification thereof, the
                    Company will make public notice of it through posting it on the bulletin board of Company&apos;s
                    website or individual notice through sending mails, fax or e-mails).
                </div>
                <div className="h1">1. Information to be collected and method of collection</div>
                <div className="h2">(1) Personal information items to be collected</div>
                <div>Personal information items to be collectedby the Company are as follows:</div>
                <div className="h3">• Information provided by the users</div>
                <div>The Company may collect the information directly provided by the users.</div>
                <div>
                    Internet membership service: ∘Name, email address, ID, telephone number, address, national
                    information, encoded identification information (CI), identification information of overlapped
                    membership (DI)∘For minors, information of legal representatives (name, birth date, CI and DI of
                    legal representatives)
                </div>
                <div>
                    Social network service: ∘Name, email address, ID, telephone number, address, national information,
                    address list (acquaintance)∘Information of place of taking pictures and date of creation of
                    files∘Information of service use of members such as the type of contents watched or used by
                    members,the persons interacting or sharing contents with members,frequencies and period of
                    activities of members
                </div>
                <div className="h3">• Information collected while the users use services</div>
                <div>
                    Besides of information directly provided by the users, the Company may collect information in the
                    course that the users use the service provided by the Company.
                </div>
                <div>
                    Equipment information: ∘Equipment identifier, operation system, hardware version, equipment set-up,
                    type and set-up of browser, use information of website or application and telephone number{' '}
                </div>
                <div>
                    Log information: ∘IP address, log data, use time, search word input by users, internet protocol
                    address, cookie and web beacon
                </div>
                <div>
                    Location information: ∘Information of device location including specific geographical location
                    detected through GPS , Bluetooth or Wifi (limited to the region permissible under the laws){' '}
                </div>
                <div>
                    Other information: ∘Preference, advertisement environment, visited pages regarding service use of
                    users
                </div>
                <div className="h2">(2) Method of collection</div>
                <div>
                    The Company collects the information of users in a way of the followings: • webpage, written form,
                    fax, telephone calling, e-mailing, tools for collection of created information • provided by partner
                    companies
                </div>
                <div className="h1">2. Use of collected information</div>
                <div>
                    The Company uses the collected information of users for the following purposes: • Member management
                    and identification • To detect and deter unauthorized or fraudulent use of or abuse of the Service•
                    Performance of contract, service fee payment and service fee settlement regarding provision of
                    services demanded by the users• Improvement of existing services and development of new services•
                    Making notice of function of company sites or applications or matters on policy change • To help you
                    connect with other users you already know and, with your permission, allow other users to connect
                    with you• To make statistics on member’s service usage, to provide services and place advertisements
                    based on statistical characteristics• To provide information on promotional events as well as
                    opportunity to participate• To comply with applicable laws or legal obligation • Use of information
                    with prior consent of the users (for example, utilization of marketing advertisement)The Company
                    agrees that it will obtain a consent from the users, if the Company desires to use the information
                    other than those expressly stated in this Policy.
                </div>
                <div className="h1">3. Disclosure of collected information </div>
                <div>
                    <div>
                        Except for the following cases, the Company will not disclose personal information with a 3rd
                        party:
                    </div>
                    <div className="h2">
                        • when the Company disclosing the information with its affiliates, partners and service
                        providers
                    </div>
                    <div>
                        When the Company&apos;s affiliates, partners and service providers carry out services such as
                        bill payment, execution of orders, products delivery and dispute resolution (including disputes
                        on payment and delivery) for and on behalf of the Company
                    </div>
                    <div className="h2">• when the users consent to disclose in advance</div>
                    <div>
                        when the user selects to be provided by the information of products and services of certain
                        companies by sharing his or her personal information with those companies
                    </div>
                    <div>
                        when the user selects to allow his or her personal information to be shared with the sites or
                        platform of other companies such as social networking sites
                    </div>
                    <div>
                        other cases where the user gives prior consent for sharing his or her personal information{' '}
                    </div>
                    <div className="h2">• when disclosure is required by the laws</div>
                    <div>if required to be disclosed by the laws and regulations; or</div>
                    <div>
                        if required to be disclosed by the investigative agencies for detecting crimes in accordance
                        with the procedure and method as prescribed in the laws and regulations
                    </div>
                </div>
                <div className="h1">4. Cookies, Beacons and Similar Technologies</div>
                <div>
                    The Company may collect collective and impersonal information through &apos;cookies&apos; or
                    &apos;web beacons&apos;.Cookies are very small text files to be sent to the browser of the users by
                    the server used for operation of the websites of the Company and will be stored in hard-disks of the
                    users&apos; computer. Web beacon is a small quantity of code which exists on the websites and
                    e-mails. By using web beacons, we may know whether an user has interacted with certain webs or the
                    contents of email. These functions are used for evaluating, improving services and setting-up
                    users&apos; experiences so that much improved services can be provided by the Company to the users
                    The items of cookies to be collected by the Company and the purpose of such collection are as
                    follows:
                </div>
                <div>
                    <div className="h2">strictly necessary cookies: </div>
                    <div>
                        This cookie is a kind of in dispensible cookie for the users to use the functions of website of
                        the Company. Unless the users allow this cookie, the services such as shopping cart or
                        electronic bill payment cannot be provided. This cookie does not collect any information which
                        may be used for marketing or memorizing the sites visited by the users (Examples of necessary
                        cookies)∘Memorize the information entered in an order form while searching other pages during
                        web browser session ∘For the page of products and check-out, memorize ordered services ∘Check
                        whether login is made on website∘Check whether the users are connected with correct services of
                        the website of the Company while the Company changes the way of operating its website∘Connect
                        the users with certain application or server ofthe services
                    </div>
                    <div className="h2">performance cookies</div>
                    <div>
                        This cookie collects information how the users use the website of the Company such as the
                        information of the pages which are visited by the users most. This data helps the Company to
                        optimize its website so that the users can search that website more comfortably. This cookie
                        does not collect any information who are the users. Any and all the information collected by
                        this cookie will be processed collectively and the anonymity will be guaranteed. (Examples of
                        performance cookies)∘Web analysis: provide statistical data on the ways of using website
                        ∘Advertisement response fee: check the effect of advertisement of the Company ∘Tracing
                        affiliated companies; one of visitors of the Company provides anonymously feedback to the
                        affiliated companies ∘Management of error: measure an error which may occur so as to give a help
                        for improving website ∘Design testing: test other design of the website of Company
                    </div>
                    <div className="h2">functionality cookies</div>
                    <div>
                        This cookie is used for memorizing the set-ups so that the Company provides services and
                        improves visit of users. Any information collected by this cookie do not identify the users
                        individually. (Examples of functionality cookies)∘Memorize set-ups applied such as layout, text
                        size, basic set-up and colors ∘Memorize when the customer respond to a survey conducted by the
                        Company
                    </div>
                    <div className="h2">targeting cookies or advertising cookies</div>
                    <div>
                        This cookie is connected with the services provided by a 3rd party such as the buttons of
                        &apos;good&apos; and &apos;share&apos;. The 3rd party provides these services by recognizing
                        that the users visits the website of the Company.(Examples of targeting cookies or advertising
                        cookies)∘carry out PR to the users as targets in other websites by connecting through social
                        networks and these networks use the information of users&apos; visit ∘provide the information of
                        users&apos; visit to ad agencies so that they can suggest an ad which may attract the interest
                        of the users
                    </div>
                </div>
                <div>
                    The users have an option for cookie installation. So, they may either allow all cookies by setting
                    option in web browser, make each cookie checked whenever it is saved, or refuses all cookies to be
                    saved: Provided that, if the user rejects the installationof cookies, it may be difficult for that
                    user to use the parts of services provided by the Company.{' '}
                </div>
                <div className="h1">5. User’s right </div>
                <div>
                    The users or their legal representatives, as main agents of the information, may exercise the
                    following rights regarding the collection, use and sharing of personal information by the Company:
                </div>
                <div>
                    • exercise right to access to personal information;• make corrections or deletion; • make temporary
                    suspension of treatment of personal information; or• request the withdrawal of their consent
                    provided before• exercise right to access to personal information;• make corrections or deletion; •
                    make temporary suspension of treatment of personal information; or• request the withdrawal of their
                    consent provided before
                </div>
                <div>
                    If, in order to exercise the above rights, you, as an user, use the menu of amendment of member
                    information of webpage or contact the Company by sending a document or e-mails, or using telephone
                    to the company(or person in charge of management of personal information or a deputy), the Company
                    will take measures without delay: Provided that the Company may reject the request of you only to
                    the extent that there exists either proper cause as prescribed in the laws or equivalent cause.
                </div>
                <div className="h1">6. Security</div>
                <div>
                    The Company regard the security of personal information of uses as very important. The company
                    constructs the following security measures to protect the users&apos; personal information from any
                    unauthorized access, release, use or modification
                </div>
                <div className="h2">Encryption of personal information </div>
                <div className="h3">
                    -Transmit users&apos; personal information by using encrypted communication zone -Store important
                    information such as passwords after encrypting it
                </div>
                <div className="h2">Countermeasures against hacking</div>
                <div className="h3">
                    -Install a system in the zone the external access to which is controlled so as to prevent leakage or
                    damage of users&apos; personal information by hacking or computer virus
                </div>
                <div className="h2">stablish and execute internal management plan</div>
                <div className="h2">Install and operate access control system</div>
                <div className="h2">Take measures to prevent forging or alteration of access record</div>
                <div className="h1">7. Protection of personal information of children</div>
                <div>
                    In principle, the Company does not collect any information from the children under 13 or equivalent
                    minimum age as prescribed in the laws in relevant jurisdiction. The website, products and services
                    of the Company are the ones to be provided to ordinary people, in principle. The website or
                    application of the Company has function to do age limit so that children cannot use it and the
                    Company does not intentionally collect any personal information from children through that function.
                    (Additional procedure for collecting personal information from children) However, if the Company
                    collects any personal information from children under 13 or equivalent minimum age as prescribed in
                    the laws in relevant jurisdiction for the services for unavoidable reason, the Company will go
                    through the additional procedure of the followings for protecting that personal information of
                    children:•verify, to the extent that efforts are reasonably made, whether they are children of the
                    age at which consent from their guardian is required and the consenting person is an authorized
                    one.• obtain consent from the parents or guardian of children so as to collect personal information
                    of children or directly send the information of products and services of the Company • give the
                    parents or guardian of children a notice of Company&apos;s policy of privacy protection for children
                    including the items, purpose and sharing of personal information collected• grant to legal
                    representatives of children a right to access to personal information of that children/correction or
                    deletion of personal information/temporary suspension of treatment of personal information/ and
                    request for withdrawal of their consent provided before• limit the amount of personal information
                    exceeding those necessary for participation in online activities
                </div>
                <div className="h1">8. Modification of Privacy Protection Policy</div>
                <div>
                    The Company has the right to amend or modify this Policy from time to time and, in such case, the
                    Company will make a public notice of it through bulletin board of its website (or through individual
                    notice such as written document, fax or e-mail) and obtain consent from the users if required by
                    relevant laws.
                </div>
                <div className="h1">10. Contact information of Company</div>
                <div>
                    Please use one of the following methods to contact the Company should you have any queriesin respect
                    to this policy or wish to update your information: •Company name : trend hunter, E-mail:
                    trend.investers@gmail.com
                </div>
            </div>
        </div>
    );
};

export default Page;
