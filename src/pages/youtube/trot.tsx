import Page from '@page-part/youtube-trot/component/0.page/Page';
import PageRootStoreConsumer from '@page-part/youtube-trot/service/store/PageRootStoreConsumer';
import React, { useEffect } from 'react';

// eslint-disable-next-line no-use-before-define
const Main = () => {
    return (
        <PageRootStoreConsumer>
            <Page />
        </PageRootStoreConsumer>
    );
};

export default Main;

export async function getStaticProps(ctx) {
    return { props: {} };
}
