import SearchQuery from '@service/SearchQuery';

interface ResultMembers {
    isDev: boolean;
}

type Result = ResultMembers;

const env: Result = {
    isDev: process.env.NODE_ENV === 'development',
};

const EnvService = (): Result => {
    const queryObj: Result = { ...env };

    if (SearchQuery.isDev === '1') {
        queryObj.isDev = true;
    } else if (SearchQuery.isDev === '0') {
        queryObj.isDev = false;
    }
    return { ...queryObj };
};

export default EnvService();
