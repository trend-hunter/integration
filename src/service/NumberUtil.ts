export const get16of9FullSizeBy100vw = (vw: number) => {
    return {
        width: vw,
        height: (vw * 9) / 16,
    };
};

export default { get16of9FullSizeBy100vw };
