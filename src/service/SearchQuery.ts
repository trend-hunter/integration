class SearchQuery {
    private readonly urlSearchParams: URLSearchParams;

    public readonly isDev: string;

    constructor() {
        try {
            if (typeof window !== 'undefined') {
                this.urlSearchParams = new URLSearchParams(location.search);
                this.isDev = this.urlSearchParams.get('isDev');
            }
        } catch (e) {
            throw new Error(e);
        }
    }
}

export default new SearchQuery();
