import fs from 'fs';

function getFiles(dir, files = []): string[] {
    const currFiles = fs.readdirSync(dir);
    // eslint-disable-next-line guard-for-in,no-restricted-syntax
    for (const i in currFiles) {
        const name = `${dir}/${currFiles[i]}`;
        if (fs.statSync(name).isDirectory()) {
            getFiles(name, files);
        } else {
            files.push(name);
        }
    }
    return files;
}

export function getChildPaths(): string[] {
    const parentPath = './src/pages';
    return getFiles(`${parentPath}`)
        .map((value) => value.replace(parentPath, '').replace('.tsx', '').replace('index', ''))
        .filter((value) => value !== '/_app' && value !== '/_document');
}
