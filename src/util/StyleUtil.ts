export const getComputedHeight = (dom: HTMLElement): number => {
    if (window === undefined) {
        return 0;
    }
    return Number(window.getComputedStyle(dom).height.replace('px', ''));
};

export const getComputedWidth = (dom: HTMLElement): number => {
    if (window === undefined) {
        return 0;
    }
    return Number(window.getComputedStyle(dom).width.replace('px', ''));
};

export default {
    getComputedHeight,
    getComputedWidth,
};
